\select@language {english}
\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Motivation}{2}{section.1.1}
\contentsline {section}{\numberline {1.2}Goal}{2}{section.1.2}
\contentsline {section}{\numberline {1.3}Application of results}{3}{section.1.3}
\contentsline {section}{\numberline {1.4}State of the art}{3}{section.1.4}
\contentsline {section}{\numberline {1.5}Technical specifications}{4}{section.1.5}
\contentsline {chapter}{\numberline {2}Metodology}{5}{chapter.2}
\contentsline {section}{\numberline {2.1}Input-output specification}{5}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Input data specification}{5}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Output data specification}{6}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}Evaluation methods}{7}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Multilayer perceptron}{7}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Convolutional neural network}{8}{subsection.2.2.2}
\contentsline {section}{\numberline {2.3}Feature selection}{9}{section.2.3}
\contentsline {part}{I\hspace {1em}Image preprocessing and training set generation}{12}{part.1}
\contentsline {chapter}{\numberline {3}Image preprocessing}{13}{chapter.3}
\contentsline {section}{\numberline {3.1}Specification of an immunohistochemistry image}{13}{section.3.1}
\contentsline {section}{\numberline {3.2}Target objects}{15}{section.3.2}
\contentsline {section}{\numberline {3.3}Image preprocessing}{17}{section.3.3}
\contentsline {subsection}{\numberline {3.3.1}Definition of 2D histogram cropping}{17}{subsection.3.3.1}
\contentsline {subsection}{\numberline {3.3.2}Shadows elimination}{18}{subsection.3.3.2}
\contentsline {subsubsection}{Efficient algorithm for finding medians}{21}{section*.19}
\contentsline {subsection}{\numberline {3.3.3}Image normalization}{28}{subsection.3.3.3}
\contentsline {chapter}{\numberline {4}Training data generation}{35}{chapter.4}
\contentsline {section}{\numberline {4.1}Application for data generation}{35}{section.4.1}
\contentsline {section}{\numberline {4.2}Workflow of application}{36}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Separation of objects}{36}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Data processing}{38}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Find minimal bounding boxes}{39}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Unification of data size}{42}{subsection.4.2.4}
\contentsline {subsection}{\numberline {4.2.5}Extracting features for MLP and preparing data for CNN}{44}{subsection.4.2.5}
\contentsline {part}{II\hspace {1em}Object classification and demo application}{45}{part.2}
\contentsline {chapter}{\numberline {5}Neural network models}{46}{chapter.5}
\contentsline {section}{\numberline {5.1}Multi layer perceptron}{46}{section.5.1}
\contentsline {subsection}{\numberline {5.1.1}Input specification}{46}{subsection.5.1.1}
\contentsline {subsection}{\numberline {5.1.2}Network specification}{47}{subsection.5.1.2}
\contentsline {section}{\numberline {5.2}Convolutional neural network}{48}{section.5.2}
\contentsline {subsection}{\numberline {5.2.1}Input specification}{48}{subsection.5.2.1}
\contentsline {subsection}{\numberline {5.2.2}Network specification}{48}{subsection.5.2.2}
\contentsline {subsection}{\numberline {5.2.3}Graphical results for the first type of input}{49}{subsection.5.2.3}
\contentsline {section}{\numberline {5.3}Compiled results}{49}{section.5.3}
\contentsline {chapter}{\numberline {6}Demo application}{54}{chapter.6}
\contentsline {chapter}{Conclusions}{56}{chapter*.41}
\contentsline {chapter}{Future directions}{57}{chapter*.42}
