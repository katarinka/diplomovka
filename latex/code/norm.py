fminmax = True
TRUEminmax = True
DOWNminmax = True
file = 'podobnosti' # podobnosti, struktury


def scale_hist(min, max, hist, maxval):
	maxval/(max-min) # step
	temp = []
	x = []
	for i in range(max-min):
		temp.append(int(hist[min + i][0]))
		
	x = np.linspace(0, max-min, num=max-min, endpoint=True)
	y = temp
	f = interp1d(x, y)
	xnew = np.linspace(0, max-min, num=maxval, endpoint=True)
	new_hist = f(xnew)
	return new_hist
	
def map_pixel(val,min, max, max_x):
	
	if (val>max):
		return max_x
	if (val < min):
		return 0
	
	return  (val - min)*(max_x /(max - min)) 
	
img = cv2.imread('images/'+file+'.png')

width = 700
height = 500
height, width, channels = img.shape
min_const = width*height*0.01 # 1%  of image resolution
if (width*height*0.01 >1000): 
	min_const = 10

if (channels > 1):
	channelR = img[:,:,2] #R
	channelG = img[:,:,1] #G
	channelB = img[:,:,0] #B
else:
	channelG = img

hist = cv2.calcHist([channelG],[0],None,[256],[0,256])
hist1 = [log(h[0]+1) for h in hist]

# FIND min and max
min = 0
count = 0;
i = 0
while (count<5) or (i==255):
	if (hist[i][0] > min_const):
		for j in range(5):
			if (hist[i+j][0] > min_const):
				count+=1
				
		if (count == 5):
			min = i
			break
		else:
			count = 0
	i+=1
		
max = 255
count = 0		
i = 255
while (count<5) or (i==255):
	if (hist[i][0] > min_const):
		for j in range(5):
			if (hist[i-j][0] > min_const):
				count+=1
				
		if (count == 5):					
			max = i
			break
		else:
			count = 0
	i-=1

print(min, max)
c_min = min
c_max = max

maxval = 255
plt.figure()
scaled_hist = scale_hist(min, max, hist, maxval)
plt.plot(scaled_hist,color = 'green')
plt.xlim([0,256])


plt.figure()
plt.axvline(x=min)
plt.axvline(x=max)
log_hist1 = cv2.GaussianBlur(np.array(hist1),(5,5),0)

# climbing up
if (TRUEminmax or DOWNminmax):
	
	i = c_min
	val1 = 0
	val2 = log_hist1[i][0]
	while (val1<val2):
		i+=1
		val1 = val2
		val2 = log_hist1[i][0]
	true_min = i

	i = c_max
	val1 = 0
	val2 = log_hist1[i][0]
	while (val1<val2):
		i-=1
		val1 = val2
		val2 = log_hist1[i][0]
	true_max = i
	
	c_min = true_min
	c_max = true_max
	plt.axvline(x=true_min)
	plt.axvline(x=true_max)

# climbing down
if (DOWNminmax):
	i = c_min + 1
	val1 = log_hist1[i - 1][0]
	val2 = log_hist1[i][0]
	while (val1>val2):
		i+=1
		val1 = val2
		val2 = log_hist1[i][0]
	down_min = i
	
	down_max = true_max - ( max - true_max)
	
	c_min = down_min
	c_max = down_max
	plt.axvline(x=down_min)
	plt.axvline(x=down_max)

max_x = 255
# mapping pixels
for i, val in enumerate(channelG):
	for j,v in enumerate(val):
		channelG[i][j] = map_pixel(v,c_min, c_max, max_x)
















