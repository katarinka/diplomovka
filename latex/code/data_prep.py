def crop_minAreaRect(img, rect):
	# rotate img
	angle = rect[2]
	rows,cols = img.shape[0], img.shape[1]
	M=cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
	img_rot = cv2.warpAffine(img,M,(cols,rows))
	# rotate bounding box
	rect0 = (rect[0], rect[1], 0.0)
	box = cv2.boxPoints(rect)
	pts = np.int0(cv2.transform(np.array([box]),M))[0]    
	pts[pts < 0] = 0
	# crop
	img_crop = 
	img_rot[pts[1][1]:pts[0][1]+1,pts[1][0]:pts[2][0]+1]
	return img_crop
	
img = cv2.fastNlMeansDenoising(img,None,20,35,51)
img = cv2.bilateralFilter(img, 40, 40,40)

ret,mask = cv2.threshold
(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
im2, contours, hierarchy = cv2.findContours
(mask,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
cnt = contours[0]
rect = cv2.minAreaRect(cnt)
img = crop_minAreaRect(img,rect)