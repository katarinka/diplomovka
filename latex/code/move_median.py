# returns True if  the intensity column of size num 
# is tall enough to move median only in that column
# position is the actual position of median in column
def enough_num(num, position, move):
	if (move == 0):
		return True
	if ((position + move) <= num and move>0):
		return True
	if ((position + move) > 0 and move<0):
		return True
	return False