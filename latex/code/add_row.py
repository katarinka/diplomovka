# h,w is actual position of pixel
# hist is histogram at position h-1,w (one row up)
def add_row(h,w):
	hist_diff = np.zeros(256,dtype=int)
	if (h+d-1 >= height): 
		return hist_diff

	# add h+d-1 th row from max(0, w-d+1) 
	# to min(width-1, w+d-1) included
	add = channel[h+d-1, max(0,w-d+1):min(width,w+d)]
	if debug:
		print('add row: ', add)
	for val in add:
		hist_diff[val] += 1

	return hist_diff