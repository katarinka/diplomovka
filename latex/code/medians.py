import os
import sys
import cv2
import numpy as np
from datetime import datetime
import math

"""
Vypocita histogram z okolia pixela (d*2) * (d*2)
Vezme median, rata s tym, ze median je svetle pozadie. -> okolie musi byt dostatocne siroke, aby to platilo
Preskaluje na 0-255 tak, ze median a mensie su 0. Cim je pixel tmavsi ako median blizsie medianu(svetlemu), tym je svetlejsi az biely.
"""

title = 'shadows_pro'
outpath = 'output/' + title + '/'
file = 'podobnosti' # struct2 test podobnosti, struktury white black sequence double_sequence random
img = cv2.imread("images/"+file+".png")
height,width,ch = img.shape
print(height,width,ch)
channel = img[:,:,1]

debug = False

"""
f = open("sample_images/" +file+ ".txt")
data = f.readlines()
f.close()
channel = [[int(dd) for dd in d.strip().split(" ")] for d in data]
channel = np.asarray(channel)
print(channel)
height = len(channel)
width = len(channel[0])
print(height, width)
"""

if not os.path.exists('output/'+title):
	print('creating path')
	os.mkdir('output/')
	os.mkdir('output/'+title)

start=datetime.now()
d = 100 # d-1 values right, left; d-1 values up down
"""
k = 10
median = np.zeros((int(h/k)+1,int(w/k)+1),dtype=int)
for i in range(int(k/2),h,k):
	print(i)
	for j in range(int(k/2),w,k):
		sub = channel[max(0,i-d):min(h,i+d),max(0,j-d):min(w,j+d)].flatten()
		median[int(i/k)][int(j/k)] = int(np.median(sub))
"""

# computes difference between hist (h,w) after removing leftest col of (h,w-1)
# h,w is actual position
def remove_col(h,w):
	hist_diff =  np.zeros(256,dtype=int)
	if (w-d<0):
		return hist_diff

	# remove w-d th col from max(0, h-d+1) to min(height-1, h+d-1) included
	rem = channel[max(0,h-d+1):min(height,h+d), w-d]
	if debug:
		print('rem col: ', rem)
	for val in rem:
		hist_diff[val] += 1
		
	return hist_diff

# computes difference between hist (h,w) and hist(h,w) after adding rightest col to (h,w)
# h,w is actual position
def add_col(h,w):
	hist_diff =  np.zeros(256,dtype=int)
	if (w+d-1 >= width):
		return hist_diff
	
	# add w+d-1 th col from max(0, h-d) to min(height-1, h+d-1) included
	add = channel[max(0,h-d+1):min(height,h+d), w+d-1]
	if debug:
		print('add col: ', add)
	for val in add:
		hist_diff[val] += 1

	return hist_diff

# h,w is actual position, hist is histogram at position h-1,w (one row up)
def add_row(h,w):
	hist_diff = np.zeros(256,dtype=int)
	if (h+d-1 >= height): 
		return hist_diff

	# add h+d-1 th row from max(0, w-d+1) to min(width-1, w+d-1) included
	add = channel[h+d-1, max(0,w-d+1):min(width,w+d)]
	if debug:
		print('add row: ', add)
	for val in add:
		hist_diff[val] += 1

	return hist_diff

# h,w is actual position, hist is histogram at position h-1,w (one row up)
def remove_row(h,w):
	hist_diff = np.zeros(256,dtype=int)
	if (h-d<0):
		return hist_diff

	# remove h-d th row from max(0, w-d+1) to min(width-1, w+d-1) included
	rem = channel[ h-d, max(0, w-d+1):min(width, w+d)]
	if debug:
		print('rem row: ', rem)
	for val in rem:
		hist_diff[val] += 1
	
	return hist_diff

# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
def remove_pix(h,w):
	
	if (h+d>height or w-d<0):
		pix1 = -1
	else:
		pix1 = channel[h+d-1,w-d]
	
	if (h-d<0 or w+d>width):
		pix2 = -1
	else:
		pix2 = channel[h-d,w+d-1]

	return pix1,pix2

# remove pixels: [h-d,w-d], [h+d-1,w+d-1]
def add_pix(h,w):
	if (h-d<0 or w-d<0):
		pix1 = -1
	else:
		pix1 = channel[h-d,w-d]
	
	if (h+d>height or w+d>width):
		pix2 = -1
	else:
		pix2 = channel[h+d-1,w+d-1]

	return pix1,pix2
	
def enough_num(num, position, move):
	if (move == 0):
		return True
	
	if ((position + move) <= num and move>0):
		return True#, index, med_num + move
		
	if ((position + move) > 0 and move<0):
		return True#, index, med_num + move
	
	return False

# moving in the new histogram
def move_median(hist, median, med_pos, move):
	if debug:
		print('stary median, pos',median,med_pos)
	if (move == 0):
		return median, med_pos
	
	if (enough_num(hist[median], med_pos, move)):
		return median, med_pos + move
	
	if (move > 0):
		move = move - (hist[median] - med_pos)
		ind = median + 1
		
		while not enough_num(hist[ind], 0, move):
			move = move - hist[ind]
			ind +=1
			if debug:
				print(ind)
		
		return ind, move
	
	move = move + med_pos
	ind = median - 1
	
	# hist[ind]+1 represents the position, when median has not yet any position and we are going from top down
	while not enough_num(hist[ind], hist[ind]+1, move):
		move = move + hist[ind]
		ind -=1
		if debug:
			print(ind)
	return ind, hist[ind] + move

# index is index of previous hist,med,med_pos
def handle_histogram(add_hist,rem_hist,index):
	num_val = num_values[index] + np.add.reduce(add_hist) - np.add.reduce(rem_hist)
	hist_diff = add_hist - rem_hist
	new_hist, median, med_pos = handle_median(hist_diff,index)
	return new_hist,median,med_pos,num_val

# index is index of previous hist,med,med_pos
def handle_median(hist_diff,index):
	median = act_medians[index] # previous median
	med_pos = meds_pos[index]  # previous median pos in hist value
	less = np.add.reduce(hist_diff[0:median]) # = move left
	more = np.add.reduce(hist_diff[median+1:256]) # = move right
	
	### equal ###
	eq = hist_diff[median]
	if (eq > 0):
		more += eq
	
	if (eq < 0):
		num_med = histos[index][median]
		if (num_med - med_pos >= -eq): # if every "going to be" deleted if above actual med_pos
			more += eq # everything above med_pos counts to more
		else: 
			more -= (num_med - med_pos) # everything above med_pos counts to more
			less -= -eq - (num_med - med_pos) # everything below or equal to med_pos counts to less
			meds_pos[index] = num_med+eq
			med_pos = meds_pos[index]
		
	move_tmp = more-less
	# keep invariant, that median is on lower position of two, when num_val is even
	if (num_values[index] % 2 == 0): # num_values[i] is not changed yet
		move = int(math.ceil(move_tmp/2))
	else:
		move = int(math.floor(move_tmp/2))
	
	new_hist = histos[index] + hist_diff
	
	if debug:
		print('move: ',move)
		print('histogram:')
		print(new_hist)
	median, med_pos = move_median(new_hist, act_medians[index], meds_pos[index], move)
	if debug:
		print('median, pozicia: ',median, med_pos)
	return new_hist, median, med_pos
	
	
# median is moved from the median position of pixel on the left or up
# i is the position of pixel left or up, against whitch is computed difference of medians
def handle_median_hist(rem_plus , rem_minus, add_plus, add_minus, i):
	#hist, act_med, med_pos
	added_val = add_plus - rem_plus + add_minus - rem_minus 
	move = add_plus + rem_plus - add_minus - rem_minus
	# to avoid +-1 mistake in position of median when move is odd and num_values was even
	if (num_values[i] % 2 == 0): # num_values[i] is not changed yet
		move = int(math.ceil(move/2))
	else:
		move = int(math.floor(move/2))
	num_val = num_values[i] + added_val
	
	median, med_pos = move_median(histos[i], act_medians[i-1], meds_pos[i-1], move)
	return median, med_pos, num_val
	

# compute median for every pixel in picture
medians = np.zeros((height,width),dtype=int)
#median = middle or lower of 2 middles

histos = np.zeros((height, 256),dtype=int)# np.array of histograms initialized to pixels in a first column, index is row
num_values = np.zeros(height,dtype=int) # number of values in histogram of considered column, index is row
act_medians = np.zeros(height,dtype=int) # actual medians of pixels of considered column, index is row
meds_pos = np.zeros(height,dtype=int) # actual positions of medians in histogram value of considered column, index is row

first = channel[0:min(d,height),0:min(d,width)].flatten()
num_values[0] = first.size
for i in first:
	histos[0][i] +=1

move = int(math.ceil(num_values[0]/2))
if debug:
	print('pozicia h,w: 0,0')
	print('histogram:')
	print(histos[0])
	print('move: ',move)
act_medians[0], meds_pos[0] = move_median(histos[0],0,0,move)
medians[0,0] = act_medians[0]
if debug:
	print('median, pozicia: ',act_medians[0], meds_pos[0])
	print('-----------')

# init the first column of histograms
for i in range(1,height):
	if debug:
		print('pozicia h,w: ',i, ' 0')
	rem_hist_diff = remove_row(i,0)
	add_hist_diff = add_row(i,0)
	histos[i],act_medians[i],meds_pos[i], num_values[i] = handle_histogram(add_hist_diff,rem_hist_diff,i-1)
	#act_medians[i], meds_pos[i], num_values[i] = handle_median(rem_plus , rem_minus, add_plus, add_minus, i-1)
	medians[i][0] = act_medians[i]
	if debug:
		print('-----------')

#sys.exit()


# (h,w)
for i in range(1,width):
	# prvy z iteho stlpca osobitne - urcit hist rozdielu
	#rem_plus , rem_minus, eq, rem_hist = remove_col(i,j, histos[i]); # TODO ma to byt i? nie i-1? ktory hist teraz upravujem?
	#add_plus, add_minus, add_hist = add_col(i,j);
	print('pozicia h,w: ',0, i)
	rem_hist_diff = remove_col(0,i)
	add_hist_diff = add_col(0,i)
	histos[0],act_medians[0],meds_pos[0], num_values[0] =handle_histogram(add_hist_diff,rem_hist_diff,0)
	medians[0][i] = act_medians[0]
	if debug:
		print('-----------')
	for j in range(1, height):
		if debug:
			print('pozicia h,w: ',j, i)
		# update diff = 
		# zvrati rem, add, vrchneho pixela
		# rem, add odoberie 2px a prida 2px
		# TODO tu treba pridat aj tie z predosleho, rem_plus , rem_minus, add_plus, add_minus
		# aby sa spravne posunul median v hist
		# TODO skontrolovat indexy v add,rem col,pix, najma w-d<0 ? <=0
		# i,j je aktualna pozicia a ratam to akoze na nej stojim. skontrolovat v col aj pix
		rem_pix1, rem_pix2 = remove_pix(j,i);
		add_pix1, add_pix2 = add_pix(j,i);
		if rem_pix1!= -1:
			rem_hist_diff[rem_pix1] += 1
		if rem_pix2!= -1:	
			rem_hist_diff[rem_pix2] += 1
		if add_pix1!= -1:
			add_hist_diff[add_pix1] += 1
		if add_pix2!= -1:
			add_hist_diff[add_pix2] += 1
		histos[j],act_medians[j],meds_pos[j], num_values[j] =handle_histogram(add_hist_diff,rem_hist_diff,j)
		medians[j][i] = act_medians[j]
		if debug:
			print('-----------')

		
print(datetime.now()-start)

cv2.imshow("channel",channel)

def pixm(v,m):
	if (v>m):
		return 255
	if (m == 0):
		print("WARNING: median is ZERO")
		return v*255
	return int(v*255/m)

print(medians)

for i in range(height):
	if debug:
		print(i)
	for j in range(width):
		m = medians[i][j]
		
		channel[i][j] =pixm(channel[i][j],m) # 255 + min(0,(channel[i][j] - m)) * 255 / m

	

cv2.imshow("new",channel)		
cv2.imshow("new",img[:,:,1])
#cv2.imshow("mediany",medians)

f = open(outpath+"medians-shadows.txt", 'w')
for m in medians:
	line = [str(mm) for mm in m]
	f.write(" ".join(line)+"\n")
f.close()

cv2.imwrite(outpath+file+'-shadows.png',channel)
cv2.imwrite(outpath+file+'-medians.png',medians)

cv2.waitKey(0)
cv2.destroyAllWindows()