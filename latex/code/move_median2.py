# hist - new histogram, median - old median
# med_pos - position of median in a hist column
def move_median(hist, median, med_pos, move):
	if (move == 0):
		return median, med_pos
	if (enough_num(hist[median], med_pos, move)):
		return median, med_pos + move
	if (move > 0):
		move = move - (hist[median] - med_pos)
		ind = median + 1
		while not enough_num(hist[ind], 0, move):
			move = move - hist[ind]
			ind +=1
		return ind, move
	move = move + med_pos
	ind = median - 1
	
	#hist[ind]+1 represents the position, when median 
	#has not yet any position and we are going from top 
	#down in the column
	while not enough_num(hist[ind], hist[ind]+1, move):
		move = move + hist[ind]
		ind -=1
		if debug:
			print(ind)
	return ind, hist[ind] + move