img = cv2.imread("image.png")
h,w,ch = img.shape
channel = img[:,:,1]
 # size of window around pixel to compute median from
d = 100

median = np.zeros((h,w),dtype=int)
for i in range(h):
	for j in range(w):
		sub = channel[max(0,i-d):min(h,i+d),
					max(0,j-d):min(w,j+d)].flatten()
		median[i][j] = int(np.median(sub))

def pixm(v,m):
	if (v>m):
		return 255    
	return int(v*255/m)

for i in range(h):
	for j in range(w):
		m = median[int(i)][int(j)]
		# channel is mapped to new values
		channel[i][j] =pixm(channel[i][j],m)