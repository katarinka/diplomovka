
\chapter{Training data generation}\label{chap:data_gen}
	
	We need to generate training data for two types of neural network - multilayer perceptron and convolutional neural network. We separate single objects from image and process them to data. In section \ref{sec:feature_sel} we decided for three types of data:
	\begin{itemize}
	\item Hu moments for MLP \citep{humoments}
	\item separated and segmented object for CNN
	\item separated object with its background and small surrounding for CNN
	\end{itemize}
All three types of data need to segment single objects from an image.
	
	\section{Application for data generation} \label{sec:train_app}
		For easier data generation we programmed simple application. This application takes preprocessed image and, in a high level view, it will:
	\begin{enumerate}
		\item segment objects according to threshold and size (not only target objects)
		\item find their minimal bounding boxes
		\item allow user to choose target and non-target objects
		\item save the data
	\end{enumerate}

	\begin{figure}
	\centering
	\includegraphics[width=.2\linewidth]{teach1.png}
	\includegraphics[width=.2\linewidth]{teach2.png}
	\includegraphics[width=.2\linewidth]{teach3.png}
	\caption{Generated data.}
	\label{fig:app_train1}
	\end{figure}	
	
	Result of this process is set of images of single objects. Example of how training data generation looks like is shown in Fig.\ref{fig:app_train}. User chooses positive and negative data by clicking in the boundary rectangles.
	
	\begin{figure}
	\centering
	\includegraphics[width=1.0\linewidth]{app_oznacene_rot1.png}
	\caption{Screenshot of the application showing objects labelled by a specialist.}
	\label{fig:app_train}
	\end{figure}
	
	After that to generate real input to the neural network, we have to extract features. More on this is said in section \nameref{sec:feat_ext}.

	\section{Workflow of application}		
	\subsection{Separation of objects}\label{susub:separation}
		As input to application we get preprocessed image. Thus we expect background and foreground is separable approximately by the same threshold for every image. We have chosen the threshold $170$. In Fig.\ref{fig:threshold} is shown mask of image how it looks like internally after the threshold has been applied. By applying this step we removed too light objects that we are certainly not target. Some darker but still too light objects remained. This is done on purpose. We need to generate not only positive target object data but also negative data. We don't know exact color intensity that separates target from false object and we expect the neural network to learn it.
	\begin{figure}
	\centering
	\includegraphics[width=.7\linewidth]{treshimg.png}
	\caption{Mask after applying threshold.}
	\label{fig:threshold}
	\end{figure}
		
To identify positions and bounding boxes for objects we take the mask and start to move through every pixel. A black pixel signifies object. From this position we initiate floodfill algorithm. We recursively ask every surrounding pixel if it is black. After identifying all black pixels we take top-, down-, right- and leftmost pixel and determine bounding box. We process this way every pixel in the image and save the object bounding boxes. In this step we also remove too small objects. Size of an object is determined by number of black pixels. Similar to lightness we don't know exact minimal size of target, but expect the network to learn it. After this step the image internally looks similar to Fig.\ref{fig:segm}. We have separated all potential target objects. 
\begin{figure}
	\centering
	\includegraphics[width=.7\linewidth]{segmentacia.png}
	\caption{Found objects.}
	\label{fig:segm}
	\end{figure}

\subsection{Data processing}\label{sec:data_proc}
We apply some smoothing filters on data which preserves edges. For the noise elimination we have chosen the OpenCV function Non-local Means Denoising algorithm \cite{ipol.2011.bcm_nlm}. This denoising algorithm removes gaussian white noise as edge-preserving smoothing filter. Parameters to filter are chosen to be $7$ pixels of the template patch that is used to compute weights,  $21$ pixels as a size of the window that is used to compute weighted average for a given pixel and $h=10$ as a parameter regulating filter strength. The second filter we apply is edge preserving Bilateral filter (\cite{Tomasi:1998:BFG:938978.939190}). We use these filters to help neural network focus on the important part of an image. Both filters are implemented in OpenCV as can be seen in Lst.\ref{lst:data_prep}.


\subsection{Find minimal bounding boxes}\label{sub:bb}
According to \nameref{sec:feature_sel} we normalize data also by rotating objects the same way. Before we proceed to find minimal bounding box for every object we have to solve the problem of multiple objects in one bounding box. This could happen, when the background of objects is too dark to be eliminated by threshold. We solve this problem with Otsu algorithm (\cite{4310076}). It is used to choose the optimal threshold value automatically to separate foreground of an image. After this step we again get mask and we have to repeat procedure of floodfilling and splitting small image into parts representing single objects. \\
To find minimal bounding box we use OpenCV function. After finding the box we have to rotate the image. Whole code for rotation of an object can be seen in Lst.\ref{lst:data_prep}. Final segmented and rotated object is shown in Fig.\ref{fig:segm}.
	
\newpage
\lstset{style=customc,caption={Data preparation},label=lst:data_prep}
\lstinputlisting[language=Python]{code/data_prep.py}


	\begin{figure}
	\centering
	\includegraphics[width=.13\linewidth]{bounding_bcgr1.png}
	\includegraphics[width=.13\linewidth]{bounding2.png}
	\includegraphics[width=.15\linewidth]{bounding_rot3.png}
	\caption{Original, segmented and rotated image.}
	\label{fig:segm}
	\end{figure}
	
	\subsection{Unification of data size}
	Our neural networks expect input of one size. We generated data of various sizes. We have chosen an input size of image to be $size_w*size_h$ $60$x$60$px. To unify the sizes of images preserving mutual ratio we find the biggest image in the generated dataset and scale its bigger side to size of input -$60$px preserving aspect ratio. We add white borders up to the size $60x60$px keeping the object in the middle. According to the ratio by which we had to reduce the biggest image we reduce all other images and add them white borders up to the size $60$x$60$px. To prevent great loss of information in small images we added square root to the ratio of $s/max$ where $s$ is one side of currently processed image and $max$ is the maximum size of image side in dataset. The resulting formula for scaling width $w$ and height $h$ is \\
	$new_w = size_w*\sqrt{(w/max)}$ and $new_h = size_h*\sqrt{(h/max)}$ \\
	
	Ratio $w/max$ and $h/max$ is always less than $1$. According to the graph of $y = \sqrt{x}$  function in Fig.\ref{fig:sqrt} resulting size will be greater compared to formula without square root. We expect neural network to learn this function. Note we have to save the $max$ size to be able to scale future images.
	
	\begin{figure}
	\centering
	\includegraphics[width=.6\linewidth]{square_root.png}
	\caption{Square root function.}
	\label{fig:sqrt}
	\end{figure}
	
Examples of resulting data are shown in Fig.\ref{fig:example_res_data}.
\begin{figure}
	\centering
	\includegraphics[width=0.15\linewidth]{objekty/data1.png}
	\includegraphics[width=0.15\linewidth]{objekty/data2.png}
	\includegraphics[width=0.15\linewidth]{objekty/data3.png}~\\
	\includegraphics[width=0.15\linewidth]{objekty/data4.png}
	\includegraphics[width=0.15\linewidth]{objekty/data5.png}
	\includegraphics[width=0.15\linewidth]{objekty/data6.png}
	\caption{Examples of resulting data.}
	\label{fig:example_res_data}
	\end{figure}
	
	\subsection{Extracting features for MLP and preparing data for CNN}\label{sec:feat_ext}
	In section \ref{sec:feature_sel} we selected features for MLP and properties of CNN input. \\
	Multilayer perceptron takes as input seven Hu moments of object. We compute them with built-in OpenCV function and normalize every dimension to have standard deviation $1$ and mean $0$. \\~\\
	Convolutional neural network takes as input 2D array representing image. We generated our data in such a way that after scaling intensities from interval $\left[0-255\right]$ to interval $\left[0-1\right]$ they are prepared to be used by the network. \\