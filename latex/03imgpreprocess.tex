\chapter{Image preprocessing}\label{chap:preproc}

	\section{Specification of an immunohistochemistry image}\label{sec:neuron_spec}
	This work deals with images that are characterized by cells placed on a light background. Some cells are marked by a chemical marker. This group of cells is darker than non-marked cells. Our aim is to separate marked objects. These are our target objects. However, not all dark objects are our target.
	
	
\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{struktury.png}
  \caption{Brain tissue with marked neurons.}
  \label{fig:sub1}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{immunuhisto_image_type.jpg}
  \caption{Tissue with marked cells}
  \label{fig:sub2}
\end{subfigure}
\caption{Examples of input images of tissues.}
\label{fig:strukturyukazka}
\end{figure}

We introduce two types of images as an example input. In Fig. \ref{fig:sub1} we can see a part of a scanned slice of a mouse brain. Small objects are neurons or their parts. Figure \ref{fig:sub2} shows another type of tissue with different type of cells. We can see that the structure of images is similar -- small dark objects on a light background. These are the properties we can rely on.

In this work we focus on images of mouse brain (like shown in Fig. \ref{fig:sub1}). All methods of preprocessing are also applicable on other types of "cell images".\\~\\
By preprocessing of an image we mean preparing image as an input for neural network. Objects are separated from the background. For an evaluation of a new, real input, the same preprocessing is applied. This is order to have as input the same type of data that was used to train the neural network.

For the image segmentation we considered all channels from RGB representation. Few observations (e.g., Fig. \ref{fig:kanale}) indicated the best contrast in green channel and that the inner structure of objects is best preserved in red channel. Blue channel was the most noisy one. 
However, the output is not much affected by the selection of a color channel. The important property is intensity. Based on these observations we take as input grayscale image created as average of green and red channel.

\begin{figure}
\centering
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{channelR.png}
  \caption{Red channel}
  \label{fig:subR}
\end{subfigure}%
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{channelG.png}
  \caption{Green channel}
  \label{fig:subG}
\end{subfigure}%
\begin{subfigure}{.33\textwidth}
  \centering
  \includegraphics[width=.95\linewidth]{channelB.png}
  \caption{Blue channel}
  \label{fig:subB}
\end{subfigure}
\caption{Three channels from RGB representation of a tissue image.}
\label{fig:kanale}
\end{figure}

\section{Target objects}
Target objects are designated by a specialist. In \ref{chap:intro} we introduced the way images are produced.
There are some important attributes that characterize target objects:
\begin{itemize}
\item the darkest objects in an image (but not all of them are target)
\item shape (as these objects are cells, they have usually similar simple shape with respect to the shape of neuron)
\item inner structure of an object (e.g., Fig. \ref{fig:sub1} mouse brain cells can contain lighter cell nucleus surrounded by the rest of a cell)
\end{itemize}
    

\begin{figure}
  \centering
  \includegraphics[width=.6\linewidth]{chvostik.png}
\caption{Examples of different structures commonly found in tissue images.}
\label{fig:difstructures}
\end{figure}
In Fig. \ref{fig:difstructures} we show few examples of object structures. Object A is our true target object. We can see a dark shade, a light cell nucleus and a projection from a cell (dendrite).

Other objects are not our target objects. These objects have risen as result of methodology for images acquisition. The brain tissue is cut into slices, so some cells are divided into parts. We present problems that can occur when distinguishing true and false objects in Fig. \ref{fig:difstructures}:
\begin{itemize}
\item objects C and D have no significant nuclei and are thin so they could be mistaken with properly shaped object E -- dendrite
\item too small object F could be mistaken with artefact G
\item object B is paced and fractioned cell that could keep some target cell properties
\item some objects could be overlapped
\end{itemize}

We deal with most of these problems the simplest way, we do not take problematic objects into account as targets. We assume that such objects are distributed uniformly, thus we don't take into account equal percentage of true target cells for every image and final result will not be affected. We take into account only objects that we are sure about to be target. This is the common approach of a scientist, who is counting the objects manually.

Methodology that generates brain images guarantees high level of readability and separability of single objects. Thus we can afford to neglect a small percentage of them.
\\~\\
Our goals for next few sections are determined by the needs of neural network and the approach we have chosen. More on why this type of preprocessing is necessary will be said in part \ref{part:objdet}. We summarize the preprocessing in following three steps:
\begin{itemize}
\item shadows elimination
\item image normalization 
\item segmentation of the objects
\end{itemize}

\section{Image preprocessing}

\subsection{Definition of 2D histogram cropping}
Before we proceed to the solution of shadows elimination and image normalization, we define cropping and scaling of a histogram.

\begin{mydef}\label{def:cropping}
\textbf{\emph{Cropping of 2D histogram.}} Let's have a 2D histogram \emph{H} with intensity scale (x-scale) from \emph{$min_x$} to \emph{$max_x$}. Let's have a constants \emph{$min_{new}$} and \emph{$max_{new}$} such that $min_x \leq min_{new}$ and $max_x \geq max_{new}$. Cropping is a mapping function $H(x)-> x'$ such that
 \[H(x) = \begin{cases} 
		min_x, ~~\emph{if}~x<=min_{new} \\
		max_x, ~~\emph{if}~x>=max_{new} \\
		(x-min_{new})\frac{max_x - min_x}{max_{new} - min_{new}}  + min_x, ~~\emph{otherwise}

   \end{cases}
\]

\end{mydef}

Cropping is an operation that shifts new boundary intensity values $min_{new}$ and $max_{new}$ to current boundary values $min_x$ and $max_x$ and the rest is uniformly distributed between them. After redistribution, $min_{new}$ become $min_x$ and $max_{new}$ become $max_x$.
Notice, that original histogram as well as cropped histogram have the same intensity scale.
\begin{figure}
  \centering
  \includegraphics[width=.6\linewidth]{cropping.png}
\caption{Cropping x-scale of histogram.}
\label{fig:cropping}
\end{figure}

~\\


\subsection{Shadows elimination}
Input images are often non-uniformly illuminated. The unequal illumination may not be evident on the first sight. Figure \ref{fig:equalization} shows evident shadows in the corners of the image after histogram equalization applied on the grayscale input image. The equalization raised the contrast of an image.

\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{channelGpodobnosti.png}
  \caption{Original image.}
  \label{fig:origeq}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{eqHistpodobnosti.png}
  \caption{Equalized histogram of image.}
  \label{fig:equalized}
\end{subfigure}
\caption{An illustration of visible shadows after equalization.}
\label{fig:equalization}
\end{figure}

Now we can introduce algorithm for shadows elimination \ref{lst:shadows}. For every pixel of one-channel image we compute the median of the surrounding matrix of pixels. Then we compute the intensity of a pixel as one (right) side x-scale cropping with value $max_{new}$ equalling the variable median value. However, our $max_{new}$ is variable for every pixel and it is the median of surrounding pixels. With respect to definition \ref{def:cropping}, every pixel higher than median is mapped to intensity 255, every other pixel is mapped linearly from (0,median) to (0,255). This guarantees that pixels with shadow background are also lightened and illumination of an image is more balanced. In Fig. \ref{fig:shadowequalization} we can see the same image as previously but with an applied filter and its equalized version. We can conclude that filter successfully eliminated shadows.
\\~\\
This approach assumes that median value belongs to background color scale, as it maps every pixel with intensity higher than median to intensity 255 - white (background is always lighter). There is a very low probability that median pixel would be key intensity of target object. It would mean that area that is covered by target objects is greater than background area. We assume this will not happen.
% keby aj bol niekde na okraji spektra targetu, stale to nevadi
\newpage
\lstset{style=customc,caption={Shadows elimination with right-max-cropping},label=lst:shadows}
\lstinputlisting[language=Python]{code/shadows.py}


~\\


\begin{figure}
\centering
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{shaelimpodobnosti.png}
  \caption{Processed green channel.}
  \label{fig:shadowschannelG}
\end{subfigure}%
\begin{subfigure}{.5\textwidth}
  \centering
  \includegraphics[width=.9\linewidth]{shaelimpodobnostiEq.png}
  \caption{Equalized histogram of green channel.}
  \label{fig:eqshachanG}
\end{subfigure}
\caption{Resulting images after removal of shadows from Fig. \ref{fig:equalization}.}
\label{fig:shadowequalization}
\end{figure}

\subsubsection{Efficient algorithm for finding medians}
In Lst. \ref{lst:shadows} we have seen algorithm for shadows elimination. This algorithm is not very efficient. The key part which takes most of the time is finding median for every pixel from its $s*s$ surrounding window. For every pixel we separately cut the window and find the median. The time complexity for this naive approach is $O(s*s*w*h)$ where $w$ and $h$ are the width and the height of the image and $s=(d+d)-1$ where $(d+d)-1$ is the length of window side. Now we introduce algorithm that computes medians from window of size $s*s$ in $O(w*h + s*w + s*h)$. As the window cannot be bigger than the image, we can say that $s*w\leq w*h$ and $s*h\leq w*h$ so the time complexity is $O(w*h)$. Notice it is not depending on the window size.

~\\
The algorithm benefits from the fact, that windows for adjacent pixels differ only in $2s$ values and these values are only from the range $0-255$. Name the adjacent pixels in a column $p_1$ and $p_2$. Let's say $d=3$ thus $2$ pixels up, down, right and left determine the window with a side $(d-1)+(d-1)+1 = (d+d-1)$. Figure \ref{fig:adjcentpix} illustrates the difference between pixels in $window_1$ and $window_2$.

\begin{figure}
  \centering
  \includegraphics[width=.3\linewidth]{medians_pic.png}
\caption{Two pixel windows used in algorithm for finding medians.}
\label{fig:adjcentpix}
\end{figure}

~\\
For every window we store its histogram - a list of $256$ values. Let's see what we have to do, when we want to retrieve median for $p_2$, if we have the $histogram_1$ of $window_1$ and we know the position of $median_1$ in this $histogram_1$. To compute the $median_2$ for $p_2$ we:
\begin{enumerate}
\item compute the difference histogram $diffhist$ - take the difference of windows marked in Fig.\ref{fig:adjcentpix}. Add green pixels and remove pink pixels. (Notice, that some values in histogram can be negative).
\item compute the $histogram_2$ for $window_2$ as $histogram_1+diffhist$
\item take the $median_1$ of $p_1$ and determine the move in the $histogram_2$ of $window_2$ from difference histogram: sum the values on the left side and right side from the $median_1$ in difference histogram and compute difference right-left denoted as $move$. (If $move$ is greater than 0 we are going to move median in the new $histogram_2$ to the right, otherwise to the left)
\item move the $median_1$ in the new $histogram_2$ $move$ positions 
\end{enumerate}
\newpage
\lstset{style=customc,caption={Add row to the difference histogram},label=lst:addrow}
\lstinputlisting[language=Python]{code/add_row.py}

The code for adding a row to $diffhist$ is displayed in Lst.\ref{lst:addrow}. Removing row works analogically. Procedure of moving median in a histogram can be seen in Lst.\ref{lst:mvmedian}. We will not include details about median moving. The important thing is that the position of median is not only the intensity value position in histogram but also the position in that concrete column of intensity. As the sum (\emph{not} number) of intensity values to the left and to the right from the median has to be the same, we have to designate the position of median also in the column of intensity.
\newpage
\lstset{style=customc,caption={Moving median in histogram},label=lst:mvmedian}
\lstinputlisting[language=Python]{code/move_median.py}
\newpage
\lstinputlisting[language=Python]{code/move_median2.py}

\noindent Step 1 takes maximum time $2s$ $\rightarrow$ $O(s)$. \\
Step 2 takes time 255. \\
Step 3 takes time 255 - summing of the histogram. \\
Step 4 takes maximum time 255. Maximum steps taken to determine new position in histogram of size $255$ is $255$.\\

So far we have $O(s)$ time complexity.\\~\\
The next step is to compute histograms and medians for every pixel in the first column of an image. This takes $O(s*h)$ time.
\begin{figure}
  \centering
  \includegraphics[width=.3\linewidth]{3pixel.png}
\caption{Four pixel windows.}
\label{fig:3pix}
\end{figure}
Following process will take into advantage fact, that if we have pixels $p_1$, $p_2$, $p_3$, $p_4$ arranged as in Fig. \ref{fig:3pix} and we know $diffhist$ of $p_1$ and $p_2$, then $diffhist$ of $p_3$ and $p_4$ will be the same except two pixels up and two pixels down. The pixels in the corners up have to be removed and down have to be added to the $diffhist$. After that if we repeat steps $2-4$ we get median for $p_4$. The difference is, that step number one took us only 4 operations $\rightarrow$ time $O(1)$.
\\~\\
Workflow of the algorithm is:
\begin{enumerate}
\item Compute medians for the pixels of the first column.
\item Repeat for every next column:	
	~\\ 2.1 Compute $diffhist$ and $median$ for the first pixel in the column (after this step we get the situation in the Fig. \ref{fig:3pix})
	~\\ 2.2 Compute $diffhist$ and $median$ for every next pixel in the column.
\end{enumerate}

\noindent Step 1 takes $O(w*s)$ time. \\
Step 2.1 takes $O(h*s)$ time. \\
Step 2.2 takes $O(h*w)$ time. \\

Time complexity of the algorithm is $O(h*w)$ as we assume that $s$ cannot be greater than width or height of an image. \\

Considering space complexity in one moment we remember histograms and median positions of one column. Thus space complexity is $(255*w+w)$ $\rightarrow$ $O(w)$.\\

This algorithm is efficient not only because of moving median instead of finding it separately for every pixel. Also, there is a high probability, that picture is not changing rapidly thus medians of adjacent pixels have very similar values. It follows that moving operation takes only small amount of time. \\

The algorithm implemented in our application is optimized to use Python numerical library Numpy thus there are some differences. The efficiency is enhanced by the fact, that summing the values in an array and addition of two arrays in Numpy is very fast.

\subsection{Image normalization}\label{sub:img_normal}
Previous shadows elimination had a side effect that our background (all intensities from the interval $<median-255>$) is now white and the other image color intensities are scaled to the interval $<0-median>$. \\~\\
Whatever color intensity image we get on the input, after the normalization target objects in every image will be approximately same intensity as well as background intensity will be the same.\\~\\
Our input image consist of two types of intensities - light, usually background and dark, usually objects. We use this fact to find approximate color intensity of both, background and target objects, and then we use cropping of 2D image histogram (see def. \ref{def:cropping}) to scale the intensities to unified values. As a result, large part of a background will be white and large part of pixels of target objects will be black.\\~\\
\begin{figure}
  \centering
  \includegraphics[width=.6\linewidth]{normhistpredGauss.png}
\caption{Histogram of an image in logarithmic scale}
\label{fig:normhistpredGauss}
\end{figure}

\noindent First we generate image histogram in logarithmic scale seen in Fig. \ref{fig:normhistpredGauss}. The logarithmic scale helps us to see dark intensities, as light -background- intensities are much more represented in the image. We can observe, that on the right side is high hill and on the left side are few small hills. These boundary intensities we are going to find. As histogram values vary and sometimes jump, we use Gaussian filter to smooth the graph. With a simple approach - starting on the edges and shifting to the middle we are looking for the first significant intensity value. On the right side it is the color of background, on the left side it is target object color. The code for localization of left boundary value is shown in Lst.\ref{lst:shifting}. Analogically we compute the right boundary except we start at $max = 255$ and move the opposite way.
\lstset{style=customc,caption={Shifting through the histogram},label=lst:shifting}
\lstinputlisting[language=Python]{code/shifting.py}

After we find the start of a hill, we assume, that up to the top there is still the same type of object. We move further until hill starts to slope (Lst.\ref{lst:up}).
\lstset{style=customc,caption={Climbing up through the histogram},label=lst:up}
\lstinputlisting[language=Python]{code/up.py}
After that we assume, that at least min(the distance we climbed up, till the nearest valley) we could climb down and it will be still the same type of object (Lst.\ref{lst:down}).
\lstset{style=customc,caption={Climbing down through the histogram},label=lst:down}
\lstinputlisting[language=Python]{code/down.py}

\noindent In Fig. \ref{fig:normhist}, we can see three points on each side representing the result of searching for boundary intensities.\\
In Fig. \ref{fig:spracovane} we show processed images with respect to min/max found in attributable phase. The histogram is scaled according to cropping definition \ref{def:cropping}.


\begin{figure}
  \centering
  \includegraphics[width=.6\linewidth]{normhist.png}
\caption{Three points, after three phases of searching.}
\medskip
\small
From outside to inside - 1. after finding min and max value of target and background, 2. after climbing up to the nearest hill, 3. after descent to the nearest valley.
\label{fig:normhist}
\end{figure}


\begin{figure}
  \centering
  \includegraphics[width=.7\linewidth]{scaled_hist_podobnosti.png}
\caption{Three phases of intensity searching: original image, after finding min/max, after upclimbing, after descent.}
\label{fig:spracovane}
\end{figure}

\begin{figure}
  \centering
  \includegraphics[width=1.0\linewidth]{scale_histogram.png}
\caption{Cropped and scaled histogram of an image in logarithmic scale.}
\label{fig:normhistpredGauss}
\end{figure}

In Fig. \ref{fig:prep_img} we show two images after two steps of preprocessing - shadows elimination and cropping and scaling histogram. At the beginning they differ in intensity and shadows. After applying preprocessing, the intensity of background and target objects is balanced in both resulting images. \\
\begin{figure}
  \centering
  \includegraphics[width=.8\linewidth]{tiene.png}
\caption{Two example images are shown in two columns. The three rows show: original images, shadows and preprocessed images. We can see that two originally different images are very similar after the preprocessing.}
\label{fig:prep_img}
\end{figure}

Until now we have not used any standard smoothing or noise elimination algorithm. These operations are very expensive especially when we want them to have an effect. We have to realize that every preprocessing step used for training data has to be used also in real application with real data.
