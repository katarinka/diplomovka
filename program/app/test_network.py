import cv2
import numpy as np
from sklearn import preprocessing
import numpy as np


def load_net():
	
	num_in = 7
	f = open("network.txt")
	data = f.readlines()
	f.close()
	#print(data)
	
	ms = data[0:num_in]
	ms = [[float(dd) for dd in d.strip().split(" ")] for d in ms]

	data = data[num_in:len(data)]
	data = [[float(dd) for dd in d.strip().split(" ")] for d in data]
	
	#data = [d.split(" ") for d in data]
	#data = [[float(dd) for dd in d] for d in data]
	num_hidden = int(data[0][0])
	num_in = int(data[0][1])
	num_out = int(data[0][2])
	
	trainedw_hidden = []
	for i in range(1,num_in+1): 
		trainedw_hidden.append(data[i])
	
	trainedb_hidden = data[num_in+1]
	
	trainedw_out = []

	for i in range(num_in+2,num_in+2+num_hidden+1):
		trainedw_out.append(data[i])
	
	trainedb_out = data[num_in+1+num_hidden+1]
	
	return create_network(ms,num_hidden, num_in, num_out,trainedw_hidden, trainedb_hidden,trainedw_out,trainedb_out)
	
def scale(x,m,s):
	#n = float(len(x))
	#m = sum(x)/n
	#s = (sum([(xx-m)**2 for xx in x])/(n-1)) ** 0.5
	return [(xx-mm)/ss for xx,mm,ss in zip(x,m,s)]
	
def extract_features(segment,ms):
	print("extracting features from segment")
	mask = segment 
	
	#m = [float(d.split(" ")[0]) for d in data]
	#s = [float(d.split(" ")[1]) for d in data]
	
	m = [float(d[0]) for d in ms]
	s = [float(d[1]) for d in ms]
	
	moments = cv2.HuMoments(cv2.moments(np.array(mask, np.float32))).flatten()
	print(moments)
	print()
	scaled = scale(moments,m,s)
	print(scaled)
	return scaled
	

	
def evaluate_segment(features, network):
	num_hidden = network["num_hidden"]
	num_in = network["num_in"]
	num_out = network["num_out"]
	w_hidden = network["w_hidden"]
	b_hidden = network["b_hidden"]
	w_out = network["w_out"]
	b_out = network["b_out"]
	
	values_hid = []
	values_out = []
	for i in range(num_hidden):
		sum = 0
		for f in range(len(features)):
			sum+= w_hidden[f][i]*features[f]
		sum += b_hidden[i]
		values_hid.append(sum)
		
	for i in range(len(values_hid)):
		if values_hid[i] < 0:
			values_hid[i] = 0
	
	#print(values_hid)
	
	for i in range(num_out):
		sum = 0
		for v in range(len(values_hid)):
			sum+= w_out[v][i]*values_hid[v]
		sum += b_out[i]
		values_out.append(sum)
	
	print(values_out)
	dist = abs(values_out[0]-values_out[1])/((abs(values_out[0])+abs(values_out[1]))/float(2))
	if dist < 0.5:
		return 1
	if values_out[0]<values_out[1]:
		return 2
	else:
		return 0
	
def create_network(ms,num_hidden, num_in, num_out, trainedw_hidden, trainedb_hidden,trainedw_out,trainedb_out): #
	network = {"ms":ms,"num_hidden" : num_hidden, "num_in" : num_in, "num_out" : num_out, "w_hidden" : trainedw_hidden, "b_hidden" : trainedb_hidden, "w_out" : trainedw_out, "b_out" : trainedb_out}
	#print(network)
	return network
	