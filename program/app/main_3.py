from PyQt4 import QtGui,QtCore
from PyQt4.QtGui import *
import sys
import datetime
import numpy as np
from scipy.interpolate import interp1d

import cv2
import tensorflow as tf

import ui_main
import segment

version = 'select'
version = 'test'
borderSize = 50

def crop_minAreaRect(img, rect):
	# rotate img
	angle = rect[2]
	rows,cols = img.shape[0], img.shape[1]
	M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
	img_rot = cv2.warpAffine(img,M,(cols,rows))
	#cv2.imshow("rot",img_rot)

	# rotate bounding box
	rect0 = (rect[0], rect[1], 0.0)
	box = cv2.boxPoints(rect)
	pts = np.int0(cv2.transform(np.array([box]), M))[0]    
	pts[pts < 0] = 0

	# crop
	return img_rot[pts[1][1]:pts[0][1]+1, pts[1][0]:pts[2][0]+1]

class App(QtGui.QMainWindow, ui_main.Ui_MainWindow):
	def __init__(self):
		super(self.__class__, self).__init__()

		self.setupUi(self)

		self.actionLoad_Image.triggered.connect(self.loadEvent)
		self.actionSave_Result.triggered.connect(self.saveEvent)
		self.actionTest_Sample.triggered.connect(self.testEvent)

		self.imageLoaded = False
		
		self.bounds = []
		self.selectedRect = -1
		
		self.btnSave.clicked.connect(self.saveEvent)
		self.btnLoad.clicked.connect(self.loadEvent)
		self.btnTest.clicked.connect(self.testEvent)
		
		if version == 'select':
			self.mousePressEvent = self.mouseClick
			self.btnTest.hide()
		if version == 'test':
			self.btnSave.hide()
	
	def rotateBounds(self,mask):
		for i in range(len(self.bounds)):
			#print(self.bounds[i]['segment'])
			size = self.bounds[i]['segment'].shape
			bounds = np.array(self.bounds[i]['segment'],dtype=np.uint8)
			bounds = cv2.copyMakeBorder(bounds, top=borderSize, bottom=borderSize, left=borderSize, right=borderSize, borderType= cv2.BORDER_CONSTANT, value=[255,255,255] )

			ret,thresh = cv2.threshold(bounds,5,255,0)
			thresh = cv2.bitwise_not(thresh)
			imgTmp, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
			cnt = contours[0]
			
			rect = cv2.minAreaRect(cnt)
			
			k = max(10,min(30,max(size)/2))
			print(k,max(size)/2,size)
			rect = ((rect[0][0],rect[0][1]),(rect[1][0]+k,rect[1][1]+k),rect[2])
			
			self.bounds[i]['rect'] = rect
			#print("setting bounds",rect)
	
	def paintEvent(self, event): 
		if not self.imageLoaded:
			return
		
		rect = QtCore.QRect(0,0,self.width(),self.height())
		qp = QPainter()
		qp.begin(self)
		#pm = self.pixMap.scaled(self.width(),self.height(), QtCore.Qt.KeepAspectRatio)

		#qp.drawPixmap(rect,pm)
		qp.drawPixmap(rect,self.pixMap)

		self.aspectx,self.aspecty = self.getAspect()

		pen = QtGui.QPen()
		pen.setColor(QtGui.QColor(168, 34, 3))
		pen.setWidth(5)
		qp.setPen(pen)
		qp.setFont(QtGui.QFont('Decorative', 20))
		
		for i,b in enumerate(self.bounds):
			color = (255,255,0)
			if "evaluated" in b:
				if b["evaluated"] == 2:
					color = (0,255,0)
				elif b["evaluated"] == 0:
					color = (255,0,0)
				elif b["evaluated"] == 1:
					color = (255,255,0)
			if self.selectedRect == i:
				pen.setWidth(7)
			else:
				pen.setWidth(3)
		
			pen.setColor(QtGui.QColor(color[0],color[1],color[2]))
			qp.setPen(pen)
			#qp.drawRect(b["lox"]*self.aspectx-3, b["loy"]*self.aspecty-3, (b["upx"]-b["lox"])*self.aspectx+6, (b["upy"]-b["loy"])*self.aspecty+6)
			
			r = b['rect']
			lox,loy = r[0]
			w,h = r[1]
			cx = lox + w/2
			cy = loy + h/2
			
			rect = QtCore.QRect(lox*self.aspectx,loy*self.aspecty,w*self.aspectx,h*self.aspecty)
			angle = r[2]

			qp.save()
			qp.translate((b["lox"]+b["upx"])/2*self.aspectx,(b["loy"]+b["upy"])/2*self.aspecty)
			qp.rotate(angle)
			qp.translate(-(cx)*self.aspectx,-(cy)*self.aspecty)
			qp.drawRect(rect)
			qp.restore()
		qp.end()
	
	def getAspect(self):
		return self.width()/self.img.shape[1],self.height()/self.img.shape[0]

	def getRectangle(self):
		for i,b in enumerate(self.bounds):
			if self.x>b["lox"]*self.aspectx and self.x<b["upx"]*self.aspectx and self.y>b["loy"]*self.aspecty and self.y<b["upy"]*self.aspecty:
				return i
		return -1
		
	def clickEventForTeach(self):
		i = self.getRectangle()
		if i == -1:
			return
			
		if i == self.selectedRect:
			if "evaluated" in self.bounds[i]:
				self.bounds[i]["evaluated"] = (self.bounds[i]["evaluated"]+1)%3
			else:
				self.bounds[i]["evaluated"] = 1
		else:
			self.selectedRect = i
			if "evaluated" not in self.bounds[i]:
				self.bounds[i]["evaluated"] = 2
		
	def mouseClick(self, event):
		if not self.imageLoaded:
			return
			
		self.x = event.pos().x()
		self.y = event.pos().y() 
		
		self.clickEventForTeach()
		
		self.update()
	
	def loadEvent(self):
		self.selectedRect = -1
		
		print("loading image")
		dlg = QtGui.QFileDialog()
		dlg.setFileMode(QFileDialog.AnyFile)
		dlg.setFilter("Images (*.jpg *.png *.tif)")
		#filenames = QtCore.QStringList()
				
		if dlg.exec_():
			filenames = dlg.selectedFiles()
			self.imgName = filenames[0]
			
		self.img = cv2.imread(self.imgName)
		
		self.img = cv2.copyMakeBorder(self.img, top=borderSize, bottom=borderSize, left=borderSize, right=borderSize, borderType= cv2.BORDER_CONSTANT, value=[255,255,255] )
		
		im_np = np.transpose(self.img, (1,0,2)).copy()

		qimage = QtGui.QImage(im_np, im_np.shape[1], im_np.shape[0], QImage.Format_RGB888)
		center = qimage.rect().center()
		matrix = QMatrix()
		matrix.translate(center.x(), center.y())
		matrix.rotate(90)
		
		transform = QTransform()
		transform.scale(-1, 1)
		
		qimage = qimage.transformed(matrix)
		qimage = qimage.transformed(transform)
		self.pixMap = QtGui.QPixmap(qimage)    
		#self.pixMap = QtGui.QPixmap(self.imgName)
		
		self.imageLoaded = True
		print("image",self.imgName,"loaded")
		self.update()
		
		mask = segment.getSegmentMask(self.img)
		self.bounds = segment.calculateSegmentBounds(mask,self.img)
		print("image",self.imgName,"segmented")
		
		self.rotateBounds(mask)
		print("image",self.imgName,"rotated")
		self.update()

	def getImages(self,onlyEvaluated):
		images,results = [],[]
		for i,b in enumerate(self.bounds):
			if onlyEvaluated:
				if "evaluated" not in b:
					continue
				if b["evaluated"] == 1: #nie je ohodnoteny
					continue
					
				res = b["evaluated"]
				if res == 2:
					res = 1
				results.append(res)
		
			print("getting images",int(100*i/len(self.bounds)),"%")
			img = self.img[b["loy"]-borderSize:b["upy"]+borderSize, b["lox"]-borderSize:b["upx"]+borderSize]
			
			img = crop_minAreaRect(img, b['rect'])
			
			img = cv2.fastNlMeansDenoising(img, None, 20, 35, 51)
			img = cv2.bilateralFilter(img, 40, 40, 40)
			images.append(img)
			
		if onlyEvaluated:
			return images,results
		return images
		
	def saveEvent(self):
		print("saving teaching data for",self.bounds.__len__(),"segments")
		self.selectedRect = -1
		
		images,results = self.getImages(True)
		for id,(img,res) in enumerate(zip(images,results)):
		
			name = datetime.datetime.now().strftime("%Y%m%d_%H%M%S_")+str(id)+"_"+str(res)
			
			#cv2.imwrite("teach_data/"+name+".png",img)
			#cv2.imwrite("teach_data/"+name+"_mask.png",255-b["segment"])

			cv2.imwrite("teach_data/"+name+"_rotated.png",img)
			
		print("teaching data for",str(id),"images saved")
		
	def resizeImg(self,img,maxi):
		size_h,size_w = 40,40

		h,w = img.shape

		sw = int(np.floor(size_w * (w/maxi) ** 0.5 ))
		sh = int(np.floor(size_h * (h/maxi) ** 0.5 ))
		add_h = size_h - sh
		add_w = size_w - sw
		add_h2 = int(add_h/2)
		add_w2 = int(add_w/2)
		img = cv2.resize(img, (sw,sh))

		img = cv2.copyMakeBorder(img, add_h2, add_h - add_h2, add_w2, add_w - add_w2, borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255, 255)) 
		return img
	
	def testEvent(self):
		if not self.imageLoaded:
			return
		
		m = interp1d([0,255],[0,1])
		
		images = self.getImages(False)
		print("images loaded")
		
		for i in range(len(images)-1,-1,-1):
			print("preparing image",i)
			img = images[i][:,:,1]
			ret,mask = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)
			
			#cv2.imshow("orig",img)
			
			seg = segment.getLargestSegment(mask)
			if seg == -1:
				del images[i]
				continue
				
			mask = seg['segment']
			mask = np.array(mask,dtype=np.uint8)

			img = img[seg["loy"]:seg["upy"]+1,seg["lox"]:seg["upx"]+1]
			
			img = cv2.bitwise_or(img,mask)
			
			img = self.resizeImg(img,204)
			
			img = cv2.bitwise_not(img)
			img = m(img)
			#cv2.imshow("final",img)
			images[i] = img.flatten()
			
		print("images prepared")
				
		with tf.Session() as sess:
			saver = tf.train.import_meta_graph('conv_model/conv_model.meta')
			saver.restore(sess, tf.train.latest_checkpoint('conv_model/'))
			#saver.restore(sess, 'conv_model/conv-model')
			
			graph = tf.get_default_graph()
			y_pred_cls = graph.get_tensor_by_name("y_pred_cls:0")
			y_pred = graph.get_tensor_by_name("y_pred:0")
			x = graph.get_tensor_by_name("x:0")
			
			print("conv model loaded")
			
			results = sess.run([y_pred_cls, y_pred], feed_dict={x: images})
			results = results[0]
			#print(results)
			for i,r in enumerate(results):
				self.bounds[i]["evaluated"] = 2*r
				
			self.update()
		
if __name__ == '__main__':
	app = QtGui.QApplication(sys.argv)
	form = App()
	form.show()
	app.exec_()
	