# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'gui-test.ui'
#
# Created by: PyQt4 UI code generator 4.11.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(800, 600)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setMaximumSize(QtCore.QSize(1000, 1000))
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.centralwidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setContentsMargins(0, 0, -1, -1)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.btnLoad = QtGui.QPushButton(self.centralwidget)
        self.btnLoad.setAutoExclusive(False)
        self.btnLoad.setObjectName(_fromUtf8("btnLoad"))
        self.horizontalLayout.addWidget(self.btnLoad)
        self.btnTest = QtGui.QPushButton(self.centralwidget)
        self.btnTest.setObjectName(_fromUtf8("btnTest"))
        self.horizontalLayout.addWidget(self.btnTest)
        spacerItem = QtGui.QSpacerItem(40, 20, QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.verticalLayout.addLayout(self.horizontalLayout)
        spacerItem1 = QtGui.QSpacerItem(20, 40, QtGui.QSizePolicy.Minimum, QtGui.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtGui.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 26))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menuMenu = QtGui.QMenu(self.menubar)
        self.menuMenu.setObjectName(_fromUtf8("menuMenu"))
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionLoad_Image = QtGui.QAction(MainWindow)
        self.actionLoad_Image.setObjectName(_fromUtf8("actionLoad_Image"))
        self.actionTest_Sample = QtGui.QAction(MainWindow)
        self.actionTest_Sample.setObjectName(_fromUtf8("actionTest_Sample"))
        self.menuMenu.addAction(self.actionLoad_Image)
        self.menubar.addAction(self.menuMenu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "Sample Selection", None))
        self.btnLoad.setText(_translate("MainWindow", "Load Image", None))
        self.btnTest.setText(_translate("MainWindow", "Test Sample", None))
        self.menuMenu.setTitle(_translate("MainWindow", "Menu", None))
        self.actionLoad_Image.setText(_translate("MainWindow", "Load Image", None))
        self.actionTest_Sample.setText(_translate("MainWindow", "Test Sample", None))

