import os
import numpy as np
import cv2 
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt

dir = "teach_data_bcgrd_notscaled/"
m = interp1d([0,255],[0,1])

def save_features(data, res):
	file = open(dir+"data.txt","a") 
	
	for d,result in zip(data, res):
		str = result
		for m in d:
			str +=  " " + repr(m)
		str +=  "\n"
		file.write(str) 
	file.close() 

data_img = []
results = []
for i,file in enumerate(os.listdir(dir)):
	if not file.endswith(".png"):
		continue
	print('processing', file)
	
	if file.endswith("1.png"):
		res = "1"
	if file.endswith("0.png"):
		res = "0"
	results.append(res)
	
	img = cv2.imread(dir + file)
	imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	imgray = cv2.bitwise_not(imgray)
	norm_img = m(imgray)
	#[print(a) for a in norm_img]
	#plt.imshow(norm_img, cmap='binary')
	#plt.show()
	data_img.append(norm_img.flatten())


save_features(data_img, results)
cv2.waitKey()
cv2.destroyAllWindows()
	

