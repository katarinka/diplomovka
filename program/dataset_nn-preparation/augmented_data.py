import numpy as np
import cv2
import os

file = 'big_image' # podobnosti_denoised_bilateral-shadows struktury_denoised_bilateral-shadows  podobnosti, struktury, podobnosti-shadows, struktury-shadows
#extension = '.tif'
title = 'augmented_data'
dir = 'teach/'
outpath = 'images/teach/' + title + '/'

if not os.path.exists('images/teach/'):
	print('creating path images/teach/')
	os.mkdir('images/teach/')

if not os.path.exists(outpath):
	print('creating path', outpath)
	os.mkdir(outpath)
	
for file in os.listdir("images/"+dir):
	if not file.endswith(tuple([".tif", ".png"])):
		continue
	print('processing', file)
	file,ext = file.split('.')
	ext = '.' + ext

	img = cv2.imread("images/"+dir+file + ext) #".png"
	height,width,ch = img.shape
	print(height,width,ch)
	channel = ((img[:,:,1])//2 + (img[:,:,2])//2) # average of green and red

	M = cv2.getRotationMatrix2D((width/2,height/2),90,1)
	channel = cv2.warpAffine(channel,M,(width,height))
	i=''
	"""
	if os.path.isfile(outpath+file+str(h)+str(w)+'.png'):
		i = 0
		while os.path.isfile(outpath+file+str(h)+str(w)+'_'+str(i)+'.png'):
			i+=1
		i = '_'+str(i
	"""
	cv2.imwrite(outpath+file[:-2]+'_rot1'+i+file[-2:]+ext,channel)
	
	channel = cv2.warpAffine(channel,M,(width,height))
	cv2.imwrite(outpath+file[:-2]+'_rot2'+i+file[-2:]+ext,channel)
	
	channel = cv2.warpAffine(channel,M,(width,height))
	cv2.imwrite(outpath+file[:-2]+'_rot3'+i+file[-2:]+ext,channel)
		
	
	
	
	