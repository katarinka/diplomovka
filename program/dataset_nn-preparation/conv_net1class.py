import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
from sklearn.metrics import confusion_matrix
from tensorflow.contrib.layers import xavier_initializer
from random import shuffle
import time
from datetime import timedelta
import math
import sys

dir = "output/" 
splitRatio = 9

### INPUT ###
def get_data():
	#f = codecs.open("datafile1.txt","r","unicode")
	f = open(dir +"data.txt")
	data = f.readlines()
	f.close()
	
	data = [[float(dd) for dd in d.strip().split(" ")] for d in data]
	
	
	#print(data)
	return data

def split(data):
	zero = [d for d in data  if d[0] == 0]
	one = [d for d in data  if d[0] == 1]
	return zero,one

def add_data(spl, num):
	sh = list(spl)
	multi = num // len(spl)
	for i in range(multi):
		shuffle(sh)
		spl.extend(sh)
	spl.extend(sh[:(num-multi*len(sh))])
	return spl

def preprocess_data(data):
	
	zero, one = split(data)
	maxi = max(len(zero), len(one))
	zero = add_data(zero, maxi - len(zero))
	one = add_data(one, maxi - len(one))
	#print(len(zero), len(one))
	
	zero.extend(one)
	shuffle(zero)
	#print(len(zero))
	return zero

def split_test_train(data):
	shuffle(data)
	p = int(len(data)/10) * splitRatio
	# test train
	return data[:p], data[p:]

data = get_data()

data = preprocess_data(data)

test, train = split_test_train(data)

test_in = [d[1:] for d in test]
test_cls = [(d[0]) for d in test]
test_out = []
for d in test:
	if d[0] == float(0):
		test_out.append([0,1])
	if d[0] == float(1):
		test_out.append([1,0])
test_out = [[(d[0])] for d in test]

test_in = np.asarray(test_in)
test_out = np.asarray(test_out)
test_cls = np.asarray(test_cls)

train_in = [d[1:] for d in train]
train_cls = [d[0] for d in train]
train_out = []
for d in train:
	if d[0] == float(0):
		train_out.append([0,1])
	if d[0] == float(1):
		train_out.append([1,0])
train_out = [[d[0]] for d in train]
		
train_in = np.asarray(train_in)
train_out = np.asarray(train_out)
train_cls = np.asarray(train_cls)
	
#[print(d) for d in train_in]
#[print(d) for d in train_out]

### INPUT ###
num_input_max = len(train_in)
batch_size = min(100,num_input_max)

def next_batch(start, end):
	if start == 0: # > num_input_max:
		# Finished epoch
		combined = list(zip(train_in, train_out))
		shuffle(combined)
		train_in[:], train_out[:] = zip(*combined)
		
		# Start next epoch
		start = 0
		# index_in_epoch= batch_size
		assert batch_size <= num_input_max
		return train_in[start:batch_size], train_out[start:batch_size]
	return train_in[start:end], train_out[start:end]



# Convolutional Layer 1.
filter_size1 = 10         #10x10px
num_filters1 = 16         

# Convolutional Layer 2.
filter_size2 = 10         # 10x10
num_filters2 = 36         

# Fully-connected layer.
fc_size = 2000             


print("Size of:")
print("- Training-set:\t\t{}".format(len(train_out)))
print("- Test-set:\t\t{}".format(len(test_out)))

img_size = 40
img_size_flat = img_size * img_size
img_shape = (img_size, img_size)
num_channels = 1

num_classes = 1

def plot_images(images, cls_true, cls_pred=None):
	assert len(images) == len(cls_true) == 9

	fig, axes = plt.subplots(3, 3)
	fig.subplots_adjust(hspace=0.3, wspace=0.3)

	for i, ax in enumerate(axes.flat):
		ax.imshow(images[i].reshape(img_shape), cmap='binary')

		# Show true and predicted classes.
		if cls_pred is None:
			xlabel = "True: {0}".format(cls_true[i])
		else:
			xlabel = "True: {0}, Pred: {1}".format(cls_true[i], cls_pred[i])

		ax.set_xlabel(xlabel)
		ax.set_xticks([])
		ax.set_yticks([])

	plt.show()


def new_weights(shape):
	
	return tf.Variable(tf.truncated_normal(shape, stddev=0.05))

def new_biases(length):
	return tf.Variable(tf.constant(0.05, shape=[length]))


def new_conv_layer(input,num_input_channels,filter_size,num_filters,use_pooling=True):
	shape = [filter_size, filter_size, num_input_channels, num_filters]

	weights = new_weights(shape=shape)

	biases = new_biases(length=num_filters)

	layer = tf.nn.conv2d(input=input,filter=weights,strides=[1, 1, 1, 1],padding='SAME')

	layer += biases

	if use_pooling:
		layer = tf.nn.max_pool(value=layer,ksize=[1, 2, 2, 1],strides=[1, 2, 2, 1],padding='SAME')

	layer = tf.nn.relu(layer)

	return layer, weights

def flatten_layer(layer):

	layer_shape = layer.get_shape()

	num_features = layer_shape[1:4].num_elements()

	layer_flat = tf.reshape(layer, [-1, num_features])

	return layer_flat, num_features

def new_fc_layer(input,num_inputs,num_outputs,use_relu=True):

	weights = new_weights(shape=[num_inputs, num_outputs])
	biases = new_biases(length=num_outputs)

	layer = tf.matmul(input, weights) + biases

	if use_relu:
		layer = tf.nn.relu(layer)

	return layer

x = tf.placeholder(tf.float32, shape=[None, img_size_flat], name='x')
x_image = tf.reshape(x, [-1, img_size, img_size, num_channels])
y_true = tf.placeholder(tf.float32, shape=[None, num_classes], name='y_true')
y_true_cls = tf.round(y_true)

layer_conv1, weights_conv1 = new_conv_layer(input=x_image,num_input_channels=num_channels,filter_size=filter_size1,num_filters=num_filters1,use_pooling=True)
print(layer_conv1)
layer_conv2, weights_conv2 = new_conv_layer(input=layer_conv1,num_input_channels=num_filters1,filter_size=filter_size2,num_filters=num_filters2,use_pooling=True)
print(layer_conv2)

layer_flat, num_features = flatten_layer(layer_conv2)
print(layer_flat)
print(num_features)


layer_fc1 = new_fc_layer(input=layer_flat, num_inputs=num_features, num_outputs=fc_size, use_relu=True)
print(layer_fc1)
layer_fc2 = new_fc_layer(input=layer_fc1,num_inputs=fc_size,num_outputs=num_classes,use_relu=False)
print(layer_fc2)

y_pred = tf.nn.sigmoid(layer_fc2)
y_pred_cls = tf.round(y_pred)

cost = tf.reduce_mean(tf.squared_difference(y_pred, y_true_cls))
optimizer = tf.train.AdamOptimizer(learning_rate=1e-4).minimize(cost)

correct_prediction = tf.equal(y_pred_cls, y_true_cls)
accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

session = tf.Session()
session.run(tf.global_variables_initializer())

train_batch_size = 64

total_iterations = 0

def optimize(num_iterations):
	global total_iterations

	for i in range(total_iterations,total_iterations + num_iterations):
		start = i % num_input_max
		end = start + train_batch_size
	
		x_batch, y_true_batch = next_batch(start,end)

		feed_dict_train = {x: x_batch,
				   y_true: y_true_batch}

		session.run(optimizer, feed_dict=feed_dict_train)
	total_iterations += num_iterations

def plot_example_errors(cls_pred, correct):
	incorrect = 1*(correct == False)
	print('incorrect:',incorrect)
	images = [im for im,cr in zip(test_in,incorrect) if cr == 1]
	images = np.concatenate((images, images))

	cls_pred = [im for im,cr in zip(cls_pred,incorrect) if cr == 1]
	cls_pred = np.concatenate((cls_pred, cls_pred)) 
	cls_true = [im for im,cr in zip(test_cls,incorrect) if cr == 1]
	cls_true = np.concatenate((cls_true, cls_true))

	plot_images(images=images[0:9],cls_true=cls_true[0:9],cls_pred=cls_pred[0:9])
	
def plot_confusion_matrix(cls_pred):
	cls_true = test_cls

	cm = confusion_matrix(y_true=cls_true,
			  y_pred=cls_pred)

	print(cm)

	plt.matshow(cm)
	num_classes = 2

	plt.colorbar()
	tick_marks = np.arange(num_classes)
	plt.xticks(tick_marks, range(num_classes))
	plt.yticks(tick_marks, range(num_classes))
	plt.xlabel('Predicted')
	plt.ylabel('True')

	plt.show()

test_batch_size = 60

def print_test_accuracy(show_example_errors=False,show_confusion_matrix=False):
	num_test = len(test_in)

	cls_pred = np.zeros(shape=[num_test,1], dtype=np.int)
	cls_pred_nround = np.zeros(shape=[num_test,1], dtype=np.int)

	i = 0

	while i < num_test:
		j = min(i + test_batch_size, num_test)

		images = test_in[i:j, :]

		labels = test_out[i:j,:]
		feed_dict = {x: images,
			     y_true: labels}

		cls_pred[i:j,:], cls_pred_nround[i:j,:]  = session.run([y_pred_cls, y_pred], feed_dict=feed_dict)
		
		i = j

	cls_true = test_cls
	cls_pred = [c[0] for c in cls_pred]
	cls_pred_nround = [c[0] for c in cls_pred_nround]
	
	correct = (cls_true == cls_pred)
	
	correct_sum = correct.sum()
	
	acc = float(correct_sum) / num_test
	msg = "Accuracy on Test-Set: {0:.1%} ({1} / {2})"
	print(msg.format(acc, correct_sum, num_test))

	if show_example_errors:
		print("Example errors:")
		plot_example_errors(cls_pred=cls_pred, correct=correct)

	if show_confusion_matrix:
		print("Confusion Matrix:")
		plot_confusion_matrix(cls_pred=cls_pred)
		
print_test_accuracy()

for i in range(2000):
	optimize(num_iterations=1) # We performed 1000 iterations above.
	print_test_accuracy(show_example_errors=False,show_confusion_matrix=False)


def plot_conv_weights(weights, input_channel=0):
	
	w = session.run(weights)

	w_min = np.min(w)
	w_max = np.max(w)

	num_filters = w.shape[3]

	num_grids = math.ceil(math.sqrt(num_filters))

	fig, axes = plt.subplots(num_grids, num_grids)

	for i, ax in enumerate(axes.flat):
		if i<num_filters:
			img = w[:, :, input_channel, i]

			ax.imshow(img, vmin=w_min, vmax=w_max,interpolation='nearest', cmap='seismic')

		ax.set_xticks([])
		ax.set_yticks([])
		
	plt.show()


def plot_conv_layer(layer, image):
	feed_dict = {x: [image]}

	values = session.run(layer, feed_dict=feed_dict)

	num_filters = values.shape[3]

	num_grids = math.ceil(math.sqrt(num_filters))

	fig, axes = plt.subplots(num_grids, num_grids)

	for i, ax in enumerate(axes.flat):
		if i<num_filters:
			img = values[0, :, :, i]

			ax.imshow(img, interpolation='nearest', cmap='binary')

		ax.set_xticks([])
		ax.set_yticks([])
	plt.show()
	
def plot_image(image):
	plt.imshow(image.reshape(img_shape),interpolation='nearest',cmap='binary')
	plt.show()


image1 = test_in[0]
plot_image(image1)


image2 = test_in[7]
plot_image(image2)

plot_conv_weights(weights=weights_conv1)
plot_conv_layer(layer=layer_conv1, image=image1)
plot_conv_layer(layer=layer_conv1, image=image2)

plot_conv_weights(weights=weights_conv2, input_channel=0)
plot_conv_weights(weights=weights_conv2, input_channel=1)
plot_conv_layer(layer=layer_conv2, image=image1)
plot_conv_layer(layer=layer_conv2, image=image2)
