import os
import numpy as np
import cv2 

### scale images to size size_h size_w 

### take image, add border (for ->), find tighest bounding rectangle, rotate, save to /bounding
### load from bounding
### find largest edge of boundary rectangle, according to largest find ratio and (with **2) scale all imgs 
### andd white border to fill size 40x40

dir = "teach_data/"
num = len(os.listdir(dir))
sizeswidth = np.zeros(num)
sizesheight = np.zeros(num)

def crop_minAreaRect(img, rect):
	# rotate img
	angle = rect[2]
	rows,cols = img.shape[0], img.shape[1]
	M = cv2.getRotationMatrix2D((cols/2,rows/2),angle,1)
	img_rot = cv2.warpAffine(img,M,(cols,rows))
	#cv2.imshow("rot",img_rot)

	# rotate bounding box
	rect0 = (rect[0], rect[1], 0.0)
	box = cv2.boxPoints(rect)
	pts = np.int0(cv2.transform(np.array([box]), M))[0]    
	pts[pts < 0] = 0

	# crop
	img_crop = img_rot[pts[1][1]:pts[0][1]+1, pts[1][0]:pts[2][0]+1]

	return img_crop

for i,file in enumerate(os.listdir(dir)):
	if not file.endswith(".png"):
		continue
	print('processing', file)
	
	img = cv2.imread(dir + file)

	bordersize = 100
	img=cv2.copyMakeBorder(img, top=bordersize, bottom=bordersize, left=bordersize, right=bordersize, borderType= cv2.BORDER_CONSTANT, value=[255,255,255] )

	imgray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
	imgray = cv2.bitwise_not(imgray)
	ret,thresh = cv2.threshold(imgray,5,255,0)
	im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

	cnt = contours[0]

	rect = cv2.minAreaRect(cnt)

	img = crop_minAreaRect(img,rect)
	
	if not os.path.exists(dir+'bounding/'):
		print('creating path')
		os.mkdir(dir+'bounding/')
	cv2.imwrite(dir+'bounding/' +'bounding_'+file,img)

	sizesheight[i] = img.shape[0]
	sizeswidth[i] = img.shape[1]
	
	
max_w = np.max(sizeswidth)
max_h = np.max(sizesheight)
print(max_h, max_w)

#sorted(segments, key = lambda x:x[0])
	
### DOWNSAMPLE, add white ###
size_h = 60
size_w = 60

ratio_h = size_h/max_h #np.ceil(
ratio_w = size_w/max_w #np.ceil(



print(ratio_h, ratio_w)

# every dimension will be divided by its ratio and complemented with white255 to size 60x60

for i,file in enumerate(os.listdir(dir + "bounding/")):
	if not file.endswith(".png"):
		continue
	print('processing', file)
	file, ext = file.split('.')
	ext = '.' + ext
	
	img = cv2.imread(dir + "bounding/" + file + ext)
	h,w,ch = img.shape
	
	maxi = max(max_w,max_h)
	print(maxi)
	
	sw = int(np.floor(size_w * (w/maxi) ** 0.5))
	sh = int(np.floor(size_h * (h/maxi) ** 0.5))
	add_h = size_h - sh
	add_w = size_w - sw
	add_h2 = int(add_h/2)
	add_w2 = int(add_w/2)
	small = cv2.resize(img, (sw,sh))
	print(h,w)
	print(sh,sw)
	print(add_h,add_w,' nb ',small.shape)
	small = cv2.copyMakeBorder(small, add_h2, add_h - add_h2, add_w2, add_w - add_w2, borderType=cv2.BORDER_CONSTANT, value=(255, 255, 255, 255)) 
	print(sh,sw,' na ',small.shape)
	
	
	
	if not os.path.exists(dir +'output/'):
		print('creating path')
		os.mkdir(dir +'output/')
	cv2.imwrite(dir+'output/' +'small_'+file+ext,small)
	

cv2.waitKey()
cv2.destroyAllWindows()