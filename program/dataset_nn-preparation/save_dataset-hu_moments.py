import numpy as np
import cv2
import os

### EXTRACT FEATURES ###
# SHAPE, rotation invariant
width = 40
height = 40

in_dir = 'output/'
out_dir = 'output_hu/'

BW = True
BW = False
if BW:
	out_dir = 'output_hu_bw/'

def process_image(f):
	img = cv2.imread(in_dir + f)
	mask = ((img[:,:,1])//2 + (img[:,:,2])//2) # average of green and red # channelG grayscale -> DNM
	
	if BW:
		mask[mask < 255] = 0

	mask = cv2.bitwise_not(mask)

	moments = cv2.HuMoments(cv2.moments(mask)).flatten()
	#print(moments)
	return moments

def scale(x,m,s):
	return [(xx-m)/s for xx in x]

def mean(x):
	n = float(len(x))
	m = sum(x)/n
	return m

def dev(x,m):
	n = float(len(x))
	s = (sum([(xx-m)**2 for xx in x])/(n-1)) ** 0.5
	return s

def normalize_data(datam):
	dat = np.array(datam)
	data = []
	norm_const = []
	for i in range(len(dat[0])):
		x = dat[:,i]
		m = mean(x)
		s = dev(x,m)
		x = scale(x,m,s)
		data.append(x)
		norm_const.append([m,s])
	
	data = np.array(data)
	data = np.transpose(data)
	print(data)
	return data, norm_const
	
def save_features(mo, res, norm_const):
	file = open(out_dir+"data.txt","w") 
	
	for c in norm_const:
		str = ""
		for cc in c:
			str += repr(cc) + " "
		str +=  "\n"
		file.write(str) 
	
	for moments,result in zip(mo, res):
		str = result
		for m in moments:
			str +=  " " + repr(m)
		str +=  "\n"
		file.write(str) 
	file.close() 
	
data_moments = []
results = []
for f in os.listdir(in_dir):
	if not f.endswith(".png"):
		continue

	if f.endswith("1.png"):
		res = "1"
	if f.endswith("0.png"):
		res = "0"
		
	results.append(res)
	print('processing',f)
	mo = process_image(f)
	data_moments.append(mo)
	
normalized, norm_const = normalize_data(data_moments)
save_features(normalized, results, norm_const)