import tensorflow as tf
from random import shuffle
import random
from tensorflow.contrib.layers import xavier_initializer
import sys

splitRatio = 2
epochs = 2000

HU = True
#HU = False
if HU:
	dir = "output_hu/"
	#dir = "output_hu_bw/"
	num_features = 7
else:
	dir = "output/"
	num_features = 1600
	
print("running from dir",dir,"features",num_features)
### INPUT ###
def get_data_HU():
	f = open(dir +"data.txt")
	data = f.readlines()
	f.close()
	
	ms = data[0:num_features]
	ms = [[float(dd) for dd in d.strip().split(" ")] for d in ms]
	#print(ms)
	
	data = data[num_features:-1]
	data = [[float(dd) for dd in d.strip().split(" ")] for d in data]
	#print(data)
	
	return data,ms
	
def get_data_img():
	f = open(dir +"data.txt")
	data = f.readlines()
	f.close()
	
	print("reading",len(data))
	
	data = [[float(dd) for dd in d.strip().split(" ")] for d in data]

	return data

def split(data):
	zero = [d for d in data  if d[0] == 0]
	one = [d for d in data  if d[0] == 1]
	return zero,one

def add_data(spl, num):
	sh = list(spl)
	multi = num // len(spl)
	for i in range(multi):
		shuffle(sh)
		spl.extend(sh)
	spl.extend(sh[:(num-multi*len(sh))])
	return spl

def preprocess_data(data):
	
	zero, one = split(data)
	#print(len(zero), len(one))
	maxi = max(len(zero), len(one))
	zero = add_data(zero, maxi - len(zero))
	one = add_data(one, maxi - len(one))
	#print(len(zero), len(one))
	
	zero.extend(one)
	shuffle(zero)
	#print(len(zero))
	return zero

def split_test_train(data):
	shuffle(data)
	p = int(len(data)/10) * splitRatio
	return data[:p], data[p:]

if HU:
	data,ms = get_data_HU()
else:
	data = get_data_img()

assert len(data) > 0

data = preprocess_data(data)

test, train = split_test_train(data)

test_in = [d[1:] for d in test]
test_out = [[d[0]] for d in test]
	
train_in = [d[1:] for d in train]
train_out = [[d[0]] for d in train]

### INPUT ###

learning_rate = 0.001
training_epochs = epochs
batch_size = 7
display_step = 1

n_hidden_1 = 500
n_hidden_2 = 500
n_input = num_features
# output is one number
n_classes = 1 #  total classes (true/false)
num_input_max = len(train_in)

# tf Graph input
x = tf.placeholder("float", [None, n_input], name="x")
y = tf.placeholder("float", [None, n_classes], name="y")

def next_batch(start, end):
	"""Return the next `batch_size` examples from this data set."""
	if start == 0: # > num_input_max:
		
		combined = list(zip(train_in, train_out))
		shuffle(combined)
		train_in[:], train_out[:] = zip(*combined)
		
		# Start next epoch
		start = 0
		index_in_epoch= batch_size
		assert batch_size <= num_input_max
		return train_in[start:batch_size], train_out[start:batch_size]
	return train_in[start:end], train_out[start:end]


# Create model
def multilayer_perceptron(x, weights, biases):
	# Hidden layer with RELU activation
	layer_1 = tf.add(tf.matmul(x, weights['h1']), biases['b1'], name="layer_1")
	layer_1 = tf.nn.relu(layer_1)
	# Hidden layer with RELU activation
	layer_2 = tf.add(tf.matmul(layer_1, weights['h2']), biases['b2'], name="layer_2")
	layer_2 = tf.nn.relu(layer_2)
	# Output layer with linear activation
	out_layer = tf.matmul(layer_2, weights['out']) + biases['out']
	out_layer = tf.nn.sigmoid(out_layer)
	return out_layer

# Store layers weight & bias
weights = {
	# tf.Variable(tf.random_normal([n_input, n_hidden_1]), name="h1"),
	'h1': tf.get_variable("h1", shape=[n_input, n_hidden_1], initializer=xavier_initializer()),
	# tf.Variable(tf.random_normal([n_input, n_hidden_1]), name="h1"),
	'h2': tf.get_variable("h2", shape=[n_hidden_1, n_hidden_2], initializer=xavier_initializer()),
	# tf.Variable(tf.random_normal([n_hidden_1, n_classes]), name="out")
	'out': tf.get_variable("out", shape=[n_hidden_2,n_classes], initializer=xavier_initializer())

}
biases = {
	'b1': tf.Variable(tf.random_normal([n_hidden_1]), name="b1"),
	'b2': tf.Variable(tf.random_normal([n_hidden_2]), name="b2"),
	'out': tf.Variable(tf.random_normal([n_classes]), name="bout")
}

# Construct model
pred = multilayer_perceptron(x, weights, biases)

cost = tf.reduce_mean(tf.squared_difference(pred, y))
optimizer = tf.train.AdamOptimizer(learning_rate=learning_rate).minimize(cost)

# Initializing the variables
init = tf.global_variables_initializer()

# Launch the graph
with tf.Session() as sess:
	sess.run(init)

	# Training cycle
	for epoch in range(training_epochs):
		avg_cost = 0.
		total_batch = int(len(train_in)/batch_size)
		index_in_epoch = 0
		# Loop over all batches
		for i in range(total_batch):
			start = index_in_epoch
			index_in_epoch += batch_size
			batch_x, batch_y = next_batch(start, index_in_epoch)
			# Run optimization op (backprop) and cost op (to get loss value)
			_, c = sess.run([optimizer, cost], feed_dict={x: batch_x, y: batch_y})
			# Compute average loss
			avg_cost += c / total_batch
			
		# Display logs per epoch step
		if epoch % display_step == 0:
			print("Epoch:", '%04d' % (epoch+1), "cost=", \
				"{:.9f}".format(avg_cost))
		
		correct_prediction = tf.equal(tf.round(pred), y)
		# Calculate accuracy
		accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
		print("Accuracy:", accuracy.eval({x: test_in, y: test_out}))
	print("Optimization Finished!")

	# Test model
	print(pred)
	print(y)
	correct_prediction = tf.equal(tf.round(pred), y)
	# Calculate accuracy
	accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
	print("Accuracy:", accuracy.eval({x: test_in, y: test_out}))
	
	### write to file ###
	trainedw_hidden = sess.run(weights.get('h1'))
	wh_temp = [' '.join(map(str, twh)) for twh in trainedw_hidden ]
	str_wh = '\n'.join(wh_temp)
	
	trainedb_hidden = sess.run(biases.get('b1'))
	str_bh =  ' '.join(map(str, trainedb_hidden))
	
	trainedw_out = sess.run(weights.get('out'))
	wout_temp = [' '.join(map(str, twout)) for twout in trainedw_out ]
	str_wout = '\n'.join(wout_temp)
	
	trainedb_out = sess.run(biases.get('out'))
	str_bout =  ' '.join(map(str, trainedb_out))
	
	network = open(dir + 'network.txt', 'w')	
	if HU:
		for c in ms:
			str_ms = ""
			for cc in c:
				str_ms += repr(cc) + " "
			str_ms +=  "\n"
			network.write(str_ms)
	
	network.write(str(n_hidden_1) + ' ' + str(n_input) + ' ' + str(n_classes) + '\n')
	# activation functions - hidden - RELU a output - linear 
	
	network.write(str_wh+ '\n')
	network.write(str_bh+ '\n')
	network.write(str_wout+ '\n')
	network.write(str_bout)

	network.close()

	#plot_example_errors(cls_pred, correct)