import cv2
import numpy as np

def pixm(v,m):
	if (v>m):
		return 255
	if (m == 0):
		print("WARNING: median is ZERO")
		return v*255
	return int(v*255/m)

def transform(channel, medians, down):
	height = len(medians)
	width = len(medians[0])
	for i in range(height):
		
		for j in range(width):
			m = medians[i][j]
			
			channel[i][j] =pixm(channel[i][j],m) # 255 + min(0,(channel[i][j] - m)) * 255 / m
	return channel
