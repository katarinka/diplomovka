import os
import sys
import cv2
import numpy as np
from datetime import datetime
import math


### Compute histogram from pixel surrounding, window of size (2d-1)*(2d-1), pixel in the middle
### Take median, expect median to be light background. -> surrounding has to be wide enough to make it true
### Scale  to 0-255 - median and less is 0. The rest linearly.


class Median:
	def __init__(self, channel, height, width, d):
		self.height, self.width = height, width
		self.d = d # d-1 values right, left; d-1 values up down
		self.channel = channel
		
		#median = middle or lower of 2 middles
		self.medians = np.zeros((height,width),dtype=int)
		self.histos = np.zeros((height, 256),dtype=int)# np.array of histograms initialized to pixels in a first column, index is row
		self.num_values = np.zeros(height,dtype=int) # number of values in histogram of considered column, index is row
		self.act_medians = np.zeros(height,dtype=int) # actual medians of pixels of considered column, index is row
		self.meds_pos = np.zeros(height,dtype=int) # actual positions of medians in histogram value of considered column, index is row
		
		self.start=datetime.now()


	# computes difference between hist (h,w) after removing leftest col of (h,w-1)
	# h,w is actual position
	def remove_col(self, h,w):
		d = self.d
		hist_diff =  np.zeros(256,dtype=int)
		if (w-d<0):
			return hist_diff

		# remove w-d th col from max(0, h-d+1) to min(height-1, h+d-1) included
		rem = self.channel[max(0,h-d+1):min(self.height,h+d), w-d]
		
		for val in rem:
			hist_diff[val] += 1
			
		return hist_diff

	# computes difference between hist (h,w) and hist(h,w) after adding rightest col to (h,w)
	# h,w is actual position
	def add_col(self, h,w):
		d = self.d
		hist_diff =  np.zeros(256,dtype=int)
		if (w+d-1 >= self.width):
			return hist_diff
		
		# add w+d-1 th col from max(0, h-d) to min(height-1, h+d-1) included
		add = self.channel[max(0,h-d+1):min(self.height,h+d), w+d-1]
		
		for val in add:
			hist_diff[val] += 1

		return hist_diff

	# h,w is actual position, hist is histogram at position h-1,w (one row up)
	def add_row(self, h,w):
		d = self.d
		hist_diff = np.zeros(256,dtype=int)
		if (h+d-1 >= self.height): 
			return hist_diff

		# add h+d-1 th row from max(0, w-d+1) to min(width-1, w+d-1) included
		add = self.channel[h+d-1, max(0,w-d+1):min(self.width,w+d)]
		
		for val in add:
			hist_diff[val] += 1

		return hist_diff

	# h,w is actual position, hist is histogram at position h-1,w (one row up)
	def remove_row(self, h,w):
		d = self.d
		hist_diff = np.zeros(256,dtype=int)
		if (h-d<0):
			return hist_diff

		# remove h-d th row from max(0, w-d+1) to min(width-1, w+d-1) included
		rem = self.channel[ h-d, max(0, w-d+1):min(self.width, w+d)]
		
		for val in rem:
			hist_diff[val] += 1
		
		return hist_diff

	# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
	def remove_pix(self, h,w):
		d = self.d
		if (h+d>self.height or w-d<0):
			pix1 = -1
		else:
			pix1 = self.channel[h+d-1,w-d]
		
		if (h-d<0 or w+d>self.width):
			pix2 = -1
		else:
			pix2 = self.channel[h-d,w+d-1]

		return pix1,pix2

	# add pixels: [h-d,w-d], [h+d-1,w+d-1]
	def add_pix(self, h,w):
		d = self.d
		if (h-d<0 or w-d<0):
			pix1 = -1
		else:
			pix1 = self.channel[h-d,w-d]
		
		if (h+d>self.height or w+d>self.width):
			pix2 = -1
		else:
			pix2 = self.channel[h+d-1,w+d-1]

		return pix1,pix2
		
	def enough_num(self, num, position, move):
		if (move == 0):
			return True
		
		if ((position + move) <= num and move>0):
			return True#, index, med_num + move
			
		if ((position + move) > 0 and move<0):
			return True#, index, med_num + move
		
		return False

	# moving in the new histogram
	def move_median(self, hist, median, med_pos, move):
		
		if (move == 0):
			return median, med_pos
		
		if (self.enough_num(hist[median], med_pos, move)):
			return median, med_pos + move
		
		if (move > 0):
			move = move - (hist[median] - med_pos)
			ind = median + 1
			
			while not self.enough_num(hist[ind], 0, move):
				move = move - hist[ind]
				ind +=1
				
			
			return ind, move
		
		move = move + med_pos
		ind = median - 1
		
		# hist[ind]+1 represents the position, when median has not yet any position and we are going from top down
		while not self.enough_num(hist[ind], hist[ind]+1, move):
			move = move + hist[ind]
			ind -=1
			
		return ind, hist[ind] + move

	# index is index of previous hist,med,med_pos
	def handle_histogram(self, add_hist,rem_hist,index):
		num_val = self.num_values[index] + np.add.reduce(add_hist) - np.add.reduce(rem_hist)
		hist_diff = add_hist - rem_hist
		new_hist, median, med_pos = self.handle_median(hist_diff,index)
		return new_hist,median,med_pos,num_val

	# index is index of previous hist,med,med_pos
	def handle_median(self, hist_diff,index):
		median = self.act_medians[index] # previous median
		med_pos = self.meds_pos[index]  # previous median pos in hist value
		less = np.add.reduce(hist_diff[0:median]) # = move left
		more = np.add.reduce(hist_diff[median+1:256]) # = move right
		
		### equal ###
		eq = hist_diff[median]
		if (eq > 0):
			more += eq
		
		if (eq < 0):
			num_med = self.histos[index][median]
			if (num_med - med_pos >= -eq): # if every "going to be" deleted if above actual med_pos
				more += eq # everything above med_pos counts to more
			else: 
				more -= (num_med - med_pos) # everything above med_pos counts to more
				less -= -eq - (num_med - med_pos) # everything below or equal to med_pos counts to less
				self.meds_pos[index] = num_med+eq
				med_pos = self.meds_pos[index]
			
		move_tmp = more-less
		# keep invariant, that median is on lower position of two, when num_val is even
		if (self.num_values[index] % 2 == 0): # num_values[i] is not changed yet
			move = int(math.ceil(move_tmp/2))
		else:
			move = int(math.floor(move_tmp/2))
		
		new_hist = self.histos[index] + hist_diff
		
		
		median, med_pos = self.move_median(new_hist, self.act_medians[index], self.meds_pos[index], move)
		
		return new_hist, median, med_pos
		
		
	# median is moved from the median position of pixel on the left or up
	# i is the position of pixel left or up, against which is computed difference of medians
	def handle_median_hist(self, rem_plus , rem_minus, add_plus, add_minus, i):
		#hist, act_med, med_pos
		added_val = add_plus - rem_plus + add_minus - rem_minus 
		move = add_plus + rem_plus - add_minus - rem_minus
		# to avoid +-1 mistake in position of median when move is odd and num_values was even
		if (self.num_values[i] % 2 == 0): # num_values[i] is not changed yet
			move = int(math.ceil(move/2))
		else:
			move = int(math.floor(move/2))
		num_val = self.num_values[i] + added_val
		
		median, med_pos = self.move_median(self.histos[i], self.act_medians[i-1], self.meds_pos[i-1], move)
		return median, med_pos, num_val
		

	def compute(self):
		height = self.height
		width = self.width
		d = self.d
		first = self.channel[0:min(d,height),0:min(d,width)].flatten()
		self.num_values[0] = first.size
		for i in first:
			self.histos[0][i] +=1

		move = int(math.ceil(self.num_values[0]/2))

		self.act_medians[0], self.meds_pos[0] = self.move_median(self.histos[0],0,0,move)
		self.medians[0,0] = self.act_medians[0]


		# init the first column of histograms
		for i in range(1,height):
			
			rem_hist_diff = self.remove_row(i,0)
			add_hist_diff = self.add_row(i,0)
			self.histos[i],self.act_medians[i],self.meds_pos[i], self.num_values[i] = self.handle_histogram(add_hist_diff,rem_hist_diff,i-1)
			#act_medians[i], meds_pos[i], num_values[i] = handle_median(rem_plus , rem_minus, add_plus, add_minus, i-1)
			self.medians[i][0] = self.act_medians[i]
			

		#sys.exit()

		# (h,w)
		for i in range(1,d):
			# prvy z iteho stlpca osobitne - urcit hist rozdielu
			# print('pozicia h,w: ',0, i)
			rem_hist_diff = self.remove_col(0,i)
			add_hist_diff = self.add_col(0,i)
			self.histos[0],self.act_medians[0],self.meds_pos[0], self.num_values[0] = self.handle_histogram(add_hist_diff,rem_hist_diff,0)
			self.medians[0][i] = self.act_medians[0]
			
			for j in range(1, d):
				#print(j)
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				add_hist_diff[self.channel[j+d-1,i+d-1]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j], self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]
					
			for j in range(d, (height-d+1)):
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				add_hist_diff[self.channel[j+d-1,i+d-1]] +=1
				rem_hist_diff[self.channel[j-d,i+d-1]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j], self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]
				
			for j in range((height-d+1),height):
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				rem_hist_diff[self.channel[j-d,i+d-1]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j], self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]

		"""
		(d)-(w-1-d+1) vpravo aj vlavo add aj rem

		0-(d-1) hore nie add a rem
		(d)-(h-1-d+1) hore aj dole add aj rem
		(h-d+1)-(h-1) dole nie add a rem
		"""
		for i in range(d, (width-d+1)):
			# prvy z iteho stlpca osobitne - urcit hist rozdielu
			#print('pozicia h,w: ',0, i)
			rem_hist_diff = self.remove_col(0,i)
			add_hist_diff = self.add_col(0,i)
			self.histos[0],self.act_medians[0],self.meds_pos[0],self.num_values[0] =self.handle_histogram(add_hist_diff,rem_hist_diff,0)
			self.medians[0][i] = self.act_medians[0]
			
			for j in range(1, d):
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				rem_hist_diff[self.channel[j+d-1,i-d]] +=1
				add_hist_diff[self.channel[j+d-1,i+d-1]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j],self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]
					
			for j in range(d, (height-d+1)):
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				add_hist_diff[self.channel[j+d-1,i+d-1]] +=1
				add_hist_diff[self.channel[j-d,i-d]] +=1
				rem_hist_diff[self.channel[j-d,i+d-1]] +=1
				rem_hist_diff[self.channel[j+d-1,i-d]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j],self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]
					
			for j in range((height-d+1),height):
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				rem_hist_diff[self.channel[j-d,i+d-1]] +=1
				add_hist_diff[self.channel[j-d,i-d]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j],self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]
				
		"""
		(w-d+1)-(w-1) vpravo nie add a rem

		0-(d-1) hore nie add a rem
		(d)-(h-1-d+1) hore aj dole add aj rem
		(h-d+1)-(h-1) dole nie add a rem
		"""
		for i in range((width-d+1),width):
			# prvy z iteho stlpca osobitne - urcit hist rozdielu
			#print('pozicia h,w: ',0, i)
			rem_hist_diff = self.remove_col(0,i)
			add_hist_diff = self.add_col(0,i)
			self.histos[0],self.act_medians[0],self.meds_pos[0],self.num_values[0] = self.handle_histogram(add_hist_diff,rem_hist_diff,0)
			self.medians[0][i] = self.act_medians[0]
			
			for j in range(1, d):
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				rem_hist_diff[self.channel[j+d-1,i-d]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j],self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]
					
			for j in range(d, (height-d+1)):
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				add_hist_diff[self.channel[j-d,i-d]] +=1
				rem_hist_diff[self.channel[j+d-1,i-d]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j], self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]
					
			for j in range((height-d+1),height):
				# remove pixels: [h+d-1,w-d], [h-d,w+d-1]
				# add pixels: [h-d,w-d], [h+d-1,w+d-1]
				add_hist_diff[self.channel[j-d,i-d]] +=1
				
				self.histos[j],self.act_medians[j],self.meds_pos[j],self.num_values[j] = self.handle_histogram(add_hist_diff,rem_hist_diff,j)
				self.medians[j][i] = self.act_medians[j]
		return self.medians

def run_medians(channel, d):
	height,width = len(channel), len(channel[0])
	print(height,width)
	M = Median(channel, height,width,d)
	medians = M.compute()
	print(datetime.now()-M.start) # podobnosti: 0:06:15.961360 0:05:59.173754 struktury: 
	return medians