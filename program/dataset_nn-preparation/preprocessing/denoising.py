
import os
from math import log
from math import floor
from random import seed
import random
import numpy as np
import cv2
from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pyplot as plt

### DENOISING IMAGE ###


file = 'podobnosti' # podobnosti, struktury, podobnosti-shadows, struktury-shadows
title = 'denoising'
outpath = 'output/' + title + '/'

if not os.path.exists('output/'+title):
	print('creating path')
	os.mkdir('output/'+title)

img = cv2.imread('images/'+file+'.png')
width = 700
height = 500
channelR = img[:,:,2] #R
channelG = img[:,:,1] #G
channelB = img[:,:,0] #B

img[:,:,0] = 0 # BGR -> blue = 0

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.resizeWindow('image', width,height)
cv2.imshow('image',channelG)

treshhold = 100
maxval = 255;
(T, threshImage) = cv2.threshold(channelG, treshhold, maxval,  cv2.THRESH_BINARY)
cv2.namedWindow('imageBEFORE', cv2.WINDOW_NORMAL)
cv2.resizeWindow('imageBEFORE', width,height)
cv2.imshow('imageBEFORE',threshImage)


#denoisedImg = cv2.fastNlMeansDenoising(channelG,None,10,7,21)
# dst = cv2.fastNlMeansDenoisingColored(img,None,10,10,7,21) pre viac kanalov - nie grayscale

#denoisedMoreImg = cv2.fastNlMeansDenoising(channelG,None,20,35,51)
#median = cv2.medianBlur(channelG,21)



#denoisedMedianImg6 = cv2.bilateralFilter(channelG, 40, 60,60)
denoisedBilateralImg = cv2.bilateralFilter(channelG, 40, 40,40)
#denoisedandBilateralImg = cv2.fastNlMeansDenoising(denoisedMedianImg4,None,16,23,27)
#denoisedMedianImg2 = cv2.bilateralFilter(channelG, 40, 20,20)

"""
(T, threshImage_more) = cv2.threshold(denoisedMoreImg, treshhold, maxval,  cv2.THRESH_BINARY)
(T, threshImage) = cv2.threshold(denoisedImg, treshhold, maxval,  cv2.THRESH_BINARY)
cv2.namedWindow('imageMORE', cv2.WINDOW_NORMAL)
cv2.resizeWindow('imageMORE', width,height)
cv2.imshow('imageMORE',threshImage)
"""
cv2.waitKey(0)

#cv2.imwrite(outpath+file+'_denoised_'+'more'+'.png',denoisedMoreImg)
#cv2.imwrite(outpath+file+'_denoised'+'.png',denoisedImg)
#cv2.imwrite(outpath+file+'_denoised_median'+'.png',median)
#cv2.imwrite(outpath+file+'_denoised_bilateral6'+'.png',denoisedMedianImg6)
cv2.imwrite(outpath+file+'_denoised_and_bilateral'+'.png',denoisedBilateralImg)
#cv2.imwrite(outpath+file+'_denoised_bilateral2'+'.png',denoisedMedianImg2)

cv2.destroyAllWindows()

