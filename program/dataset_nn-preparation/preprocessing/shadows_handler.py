import os
import sys
import cv2
import numpy as np
from datetime import datetime
import math

import pixel_medians
import downsample
import shadows

# Combines algorithm for 
# downsampling image, 
# finding pixel medians with window (2d-1)*(2d-1)
# tranforming image to eliminate shadows using medians of downsampled image

title = 'shadows_handler'
outpath = 'output/' + title + '/'
file = 'at8 10x C' # struktury_denoised_bilateral podobnosti_denoised_bilateral struct2 test podobnosti, struktury white black sequence double_sequence random

img = cv2.imread("images/vyber/"+file+".tif")
height,width,ch = img.shape
print(height,width,ch)
channel = ((img[:,:,1])//2 + (img[:,:,2])//2) # average of green and red
"""
f = open("sample_images/" +file+ ".txt")
data = f.readlines()
f.close()
channel = [[int(dd) for dd in d.strip().split(" ")] for d in data]
channel = np.asarray(channel)
print(channel)
height = len(channel)
width = len(channel[0])
print(height, width)
"""

if not os.path.exists('output/'):
	print('creating path output/')
	os.mkdir('output/')
if not os.path.exists('output/'+title):
	p = 'output/'+title
	print('creating path ', p)
	os.mkdir('output/'+title)

cv2.imshow("channel",channel)
down = 1
down_channel = downsample.basicdownsample(channel, down)

### medians ###
d = min(100, max(width, height))
if d>height and d>width:
	d = max(height, width)
d = 100
print('d:',d)
	
medians = pixel_medians.run_medians(down_channel,d)
print("medians: ",medians)

### transformation to eliminate shadows ###
new_channel = shadows.transform(channel, medians, down)

cv2.imshow("new",new_channel)		

### saving computed medians and image
f = open(outpath+"medians-"+file+"_down" + str(down) +"_d" + str(d)+ "_shadows" + ".txt", 'w')
for m in medians:
	line = [str(mm) for mm in m]
	f.write(" ".join(line)+"\n")
f.close()

cv2.imwrite(outpath+file+'-shadows.png',channel)
cv2.imwrite(outpath+file+'-medians.png',medians)

cv2.waitKey(0)
cv2.destroyAllWindows()