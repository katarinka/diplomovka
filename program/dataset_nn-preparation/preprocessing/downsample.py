import numpy as np

# functions to downsample image channel

def basicdownsample(channel, down):
	down_channel = channel[::down,::down]
	print('channel, down', channel.shape, down)
	print('down_channel', down_channel.shape)
	return down_channel