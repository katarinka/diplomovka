import os
from math import log
from math import floor
from random import seed
import random
import numpy as np
import cv2
from scipy.interpolate import interp1d
from scipy import signal

import matplotlib as mpl
import matplotlib.pyplot as plt

### IMAGE PREPROCESSING - filter+treshold ###

minmax = True
TRUEminmax = True
DOWNminmax = True
file = 'at8 10x D'# at8 10x C-shadows at8 10x-shadows 'at8-10x-B-shadows' # podobnosti_denoised_bilateral-shadows struktury_denoised_bilateral-shadows  podobnosti, struktury, podobnosti-shadows, struktury-shadows at8-10x-B-shadows at8-10x-D-shadows
title = '0image_processing'
outpath = 'output/' + title + '/'

def scale_hist(min, max, hist, maxval):
	maxval/(max-min) # aky je krok
	temp = []
	x = []
	for i in range(max-min):
		temp.append(int(hist[min + i][0]))
		
	x = np.linspace(0, max-min, num=max-min, endpoint=True)
	y = temp
	
	print(x,y)
	print(len(x),len(y))
	
	f = interp1d(x, y)
	xnew = np.linspace(0, max-min, num=maxval, endpoint=True)
	new_hist = f(xnew)
	
	return new_hist
		
def map_pixel(val,min, max, max_x):
	
	if (val>max):
		return max_x
	if (val < min):
		return 0
	
	return  (val - min)*(max_x /(max - min)) #val/max_x * (max - min) + min
	
	#(XX- min_x)/(max_x - min_x)* (max - min) + min
	#XX = (val-min)*(max_x - min_x)/(max - min)  + min_x


### EXTRACT FEATURES ###
img = cv2.imread('images/vyber/'+file+'.tif')

width = 700
height = 500
height, width, channels = img.shape

min_const = width*height*0.001 # 0.1% z rozlisenia obrazku (asi mozem aj zvysit)
if (width*height*0.01 >1000): 
	min_const = 10

if (channels > 1):
	channelR = img[:,:,2] #R
	channelG = img[:,:,1] #G
	channelB = img[:,:,0] #B
	channel = ((img[:,:,1])//2 + (img[:,:,2])//2) # average of green and red
else:
	channel = img

print("denoising image")
#channel = cv2.fastNlMeansDenoising(channel,None,20,35,51)
#channel = cv2.medianBlur(channel,31)
print("image denoised")
#cv2.imwrite('images/proc_struktury.png',channel)

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.resizeWindow('image', width,height) # neorezava, skaluje
#cv2.imshow('image',channel)

# povodny histogram
hist = cv2.calcHist([channel],[0],None,[256],[0,256])


log_hist = [log(h[0]+1) for h in hist]

log_hist_med = signal.medfilt(log_hist,5)
log_hist_blur = cv2.GaussianBlur(np.array(log_hist_med),(5,5),0)


min = 0
count = 0;
# aspon niekolko ich musi ist za sebou, aby bolo jasne, ze to nie je len nejaky artefakt
i = 0
while (count<5) or (i==255):
	if (hist[i][0] > min_const):
		for j in range(5):
			if (hist[i+j][0] > min_const):
				count+=1
				
		if (count == 5):
			min = i
			break
		else:
			count = 0
	i+=1
		
max = 255
count = 0		
i = 255
while (count<5) or (i==255):
	if (hist[i][0] > min_const):
		for j in range(5):
			if (hist[i-j][0] > min_const):
				count+=1
				
		if (count == 5):					
			max = i
			break
		else:
			count = 0
	i-=1

print(min, max)
c_min = min
c_max = max
plt.figure()
plt.axvline(x=min)
plt.axvline(x=max)

if (TRUEminmax or DOWNminmax):
	
	i = c_min
	val1 = 0
	val2 = log_hist_blur[i][0]
	while (val1<val2):
		i+=1
		val1 = val2
		val2 = log_hist_blur[i][0]
	true_min = i

	i = c_max
	val1 = 0
	val2 = log_hist_blur[i][0]
	while (val1<val2):
		i-=1
		val1 = val2
		val2 = log_hist_blur[i][0]
	true_max = i
	
	c_min = true_min
	c_max = true_max
	plt.axvline(x=true_min)
	plt.axvline(x=true_max)
	
if (DOWNminmax):
	i = c_min + 1
	val1 = log_hist_blur[i - 1][0]
	val2 = log_hist_blur[i][0]
	while (val1>val2):
		i+=1
		val1 = val2
		val2 = log_hist_blur[i][0]
	down_min = i
	
	down_max = true_max - ( max - true_max)
	
	c_min = down_min
	c_max = down_max
	plt.axvline(x=down_min)
	plt.axvline(x=down_max)


max_x = 255

print("started converting")
for i, val in enumerate(channel):
	for j,v in enumerate(val):
		channel[i][j] = map_pixel(v,c_min, c_max, max_x)
print("conversion complete")

scaled_hist = cv2.calcHist([channel],[0],None,[256],[0,256])
scaled_hist = scaled_hist[0:255]
log_scaled_hist = [log(h[0]+1) for h in scaled_hist]

plt.plot(log_hist,color = 'blue', label='log histogram')
plt.plot(log_hist_blur,color = 'red', label='blurred log histogram')
plt.plot(log_scaled_hist,color = 'green', label='scaled log histogram')
plt.legend(loc='upper left', shadow=True, fontsize='medium')
plt.xlim([0,255])

plt.figure()
#scaled_hist = scale_hist(c_min, c_max, hist, max_x)
#plt.plot(scaled_hist,color = 'green', label='scaled_hist')
plt.plot(hist,color = 'blue', label='original_hist')
plt.plot(scaled_hist,color = 'green', label='scaled_hist')
plt.legend(loc='upper left', shadow=True, fontsize='x-large')
plt.title('scaled_hist + original_hist')
plt.xlim([0,256])

cv2.namedWindow('image_norm', cv2.WINDOW_NORMAL)
cv2.resizeWindow('image_norm', width,height) # neorezava, skaluje
#cv2.imshow('image_norm',channel)

if not os.path.exists('output/'+title):
	print('creating path')
	os.mkdir('output/'+title)
if (DOWNminmax):
	i=''
	if os.path.isfile(outpath+'norm_'+file+'DOWN.png'):
		i = 0
		while os.path.isfile(outpath+'norm_'+file+'DOWN'+str(i)+'.png'):
			i+=1
	cv2.imwrite(outpath+'norm_'+file+'2DOWN'+str(i)+'.png',channel)
if (TRUEminmax and not DOWNminmax):
	i=''
	if os.path.isfile(outpath+'norm_'+file+'TRUE.png'):
		i = 0
		while os.path.isfile(outpath+'norm_'+file+'TRUE'+str(i)+'.png'):
			i+=1
	cv2.imwrite(outpath+'norm_'+file+'1TRUE'+str(i)+'.png',channel)
if (minmax and not TRUEminmax and not DOWNminmax):
	i=''
	if os.path.isfile(outpath+'norm_'+file+'.png'):
		i = 0
		while os.path.isfile(outpath+'norm_'+file+str(i)+'.png'):
			i+=1
	cv2.imwrite(outpath+'norm_'+file+str(i)+'0.png',channel)

plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()