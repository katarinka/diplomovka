from math import exp
import numpy as np
import cv2
import os
import segment
from sklearn import preprocessing
import numpy as np

### apply THRESH_OTSU to saved small image
### find the largest from segmented objects
### cut the largest and save (with white boundary = original size)

title = 'teach_data_proc'
dir = 'teach/'
#file = 'proc_20180423_180216_6_1'# proc_20180423_180216_1_1
outpath = 'output/' + title + '/'

if not os.path.exists('output/'+title):
	print('creating path')
	os.mkdir('output/'+title)

def floodfill(i0,j0,img):
	ny,nx = img.shape[0],img.shape[1]
	todo = [(i0,j0)]
	mask = np.ones((ny,nx), dtype = np.uint8)*255
	size = 0
	while len(todo) != 0:
		t = todo[-1]
		del todo[-1]
		i,j = t[0],t[1]
		mask[i,j] = 0
		size +=1
		if i>0:
			if img[i-1][j] == 0:
				todo.append((i-1,j))
				img[i-1][j] = 255
		if i<ny-1:
			if img[i+1][j] == 0:
				todo.append((i+1,j))
				img[i+1][j] = 255
		if j>0:
			if img[i][j-1] == 0:
				todo.append((i,j-1))
				img[i][j-1] = 255
		if j<nx-1:
			if img[i][j+1] == 0:
				todo.append((i,j+1))
				img[i][j+1] = 255
	
	return (size, mask)
	
def calculateSegments(mask):
	# returns rectangles for individual segements 
	mask = mask.copy()
	segments = []
	for i in range(mask.shape[0]):
		for j in range(mask.shape[1]):
			if mask[i][j] != 0:
				continue
			segment = floodfill(i,j,mask)
	
			if segment[0] > 15:
				segments.append(segment)
	return segments

def getLargestSegment(mask):
	segments = calculateSegments(mask)
	#segments = np.sort(segments,axis=0)
	segments = sorted(segments, key = lambda x:x[0])
	#print(segments)
	return segments[-1]

for f in os.listdir(dir):
	if not f.endswith(".png"):
		continue
	print('processing',f)

	file = f.split('.')[0]
	img = cv2.imread(dir + f)
	height, width, channels = img.shape

	if (channels > 1):
		channelR = img[:,:,2] #R
		channelG = img[:,:,1] #G
		channelB = img[:,:,0] #B
		channel = ((img[:,:,1])//2 + (img[:,:,2])//2) # average of green and red
	else:
		channel = img
	
	denoisedMoreImg = cv2.fastNlMeansDenoising(channel,None,20,35,51)
	denoisedBilateralImg = cv2.bilateralFilter(denoisedMoreImg, 40, 40,40)
	
	ret3,mask = cv2.threshold(denoisedBilateralImg,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)


	mask = getLargestSegment(mask)[1]
	mask_inv = cv2.bitwise_not(mask)

	
	thresh_image = cv2.bitwise_or(channel,mask)

	cv2.imwrite(outpath+file +'.png',channel)
	
	i=''
	if os.path.isfile(outpath+file+'_mask_OTSU.png'):
		i = 0
		while os.path.isfile(outpath+file+'_mask_OTSU'+str(i)+'.png'):
			i+=1
	cv2.imwrite(outpath+file+'_mask_OTSU'+str(i)+'.png',mask)

	i=''
	if os.path.isfile(outpath+file+'_thresh_OTSU.png'):
		i = 0
		while os.path.isfile(outpath+file+'_thresh_OTSU'+str(i)+'.png'):
			i+=1
	cv2.imwrite(outpath+file+'_thresh_OTSU'+str(i)+'.png',thresh_image)


cv2.waitKey()
cv2.destroyAllWindows()