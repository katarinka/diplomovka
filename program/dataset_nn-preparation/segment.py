import cv2
import numpy as np

minSegmentSizeInPix = 500

def getSegmentMask(image):
	image = ((image[:,:,1])//2 + (image[:,:,2])//2) # average of green and red
	#img[:,:,0] = 0 # BGR -> blue = 0

	#img = cv2.fastNlMeansDenoising(img,None,20,35,51)
	#img = cv2.bilateralFilter(img, 40, 40,40)
	threshold,maxval = 160,255
	(T, mask) = cv2.threshold(img, threshold, maxval,  cv2.THRESH_BINARY)

	#cv2.imshow('segmented',mask)
	return mask
	
def floodfill(i0,j0,mask):
	ny,nx = mask.shape[0],mask.shape[1]
	todo = [(i0,j0)]
	lox,upx = j0,j0
	loy,upy = i0,i0
	size = 1
	segmentPairs = []
	while todo.__len__() != 0:
		t = todo[-1]
		del todo[-1]
		size += 1
		i,j = t[0],t[1]
		segmentPairs.append((i,j))
		if i>0:
			if mask[i-1][j] == 0:
				todo.append((i-1,j))
				mask[i-1][j] = 255
		if i<ny-1:
			if mask[i+1][j] == 0:
				todo.append((i+1,j))
				mask[i+1][j] = 255
		if j>0:
			if mask[i][j-1] == 0:
				todo.append((i,j-1))
				mask[i][j-1] = 255
		if j<nx-1:
			if mask[i][j+1] == 0:
				todo.append((i,j+1))
				mask[i][j+1] = 255
		
		if i<loy:
			loy = i
		if i>upy:
			upy = i
		if j<lox:
			lox = j
		if j>upx:
			upx = j
	
	if size < minSegmentSizeInPix:
		return -1
	segment = 255*np.ones((upy+1-loy,upx+1-lox),dtype=int)
	for s in segmentPairs:
		segment[s[0]-loy][s[1]-lox] = 0
	return {"lox":lox,"upx":upx,"loy":loy,"upy":upy,"segment":segment}
	
def calculateSegmentBounds(mask):
	# returns rectangles for individual segements
	
	mask = mask.copy()
	bounds = []
	for i in range(mask.shape[0]):
		for j in range(mask.shape[1]):
			if mask[i][j] != 0:
				continue
			bnd = floodfill(i,j,mask)
			if bnd != -1:
				bounds.append(bnd)
	print("found bounds for ",bounds.__len__(),"segments")
	return bounds
			
def getSampleMask():
	mask = cv2.imread("input_data/img.png",0) 
	
	for i in range(mask.shape[0]):
		for j in range(mask.shape[1]):
			if mask[i][j] < 128:
				mask[i][j] = 0
			else:
				mask[i][j] = 255
	print("sample mask loaded")
	return mask
	
def loadSegments(self):
	mask = cv2.imread("tmp/segments.png")
		
if __name__ == "__main__":
	#mask = getSampleMask()
	img = cv2.imread("input_data/struktury.png") 
	mask = getSegmentMask(img)
	#cv2.imshow('segment mask',mask)
	#bounds = calculateSegmentBounds(mask)