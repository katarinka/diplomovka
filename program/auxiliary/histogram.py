import os
from math import log
from math import floor
from random import seed
import random
import numpy as np
import cv2
from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pyplot as plt

file = 'black25' # norm_podobnosti-shadowsDOWN, struktury, podobnosti, norm_struktury-shadowsDOWN 
title = 'hist'
outpath = 'output/' + title + '/'

### EXTRACT FEATURES ###
img = cv2.imread('images/'+file+'.png')

width = 600
height = 400
height, width, channels = img.shape

min_const = width*height*0.01 # 1% z rozlisenia obrazku (asi mozem aj zvysit)
if (width*height*0.01 >1000): 
	min_const = 10

if (channels > 1):
	channelR = img[:,:,2] #R
	channelG = img[:,:,1] #G
	channelB = img[:,:,0] #B
	channel = ((img[:,:,1])//2 + (img[:,:,2])//2) # average of green and red
else:
	channel = img

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.resizeWindow('image', width,height) # neorezava, skaluje
cv2.imshow('image',channel)

# povodny histogram
hist = cv2.calcHist([channel],[0],None,[256],[0,256])

treshold = 120
maxval = 255;
(T, threshImage) = cv2.threshold(channel, treshold, maxval,  cv2.THRESH_BINARY)
cv2.namedWindow('treshIMG', cv2.WINDOW_NORMAL)
cv2.resizeWindow('treshIMG', width,height)
cv2.imshow('treshIMG',threshImage)

plt.plot(hist,color = 'red')
plt.title('hist')
plt.xlim([0,256])


if not os.path.exists('output/'):
	print('creating path')
	os.mkdir('output/')
if not os.path.exists('output/'+title):
	print('creating path')
	os.mkdir('output/'+title)

i=''
if os.path.isfile(outpath+file+'_hist'+'.png'):
	i = 0
	while os.path.isfile(outpath+file+'_hist'+str(i)+'.png'):
		i+=1
plt.savefig(outpath+file+str(i)+'.png')



cv2.waitKey(0)
cv2.destroyAllWindows()















