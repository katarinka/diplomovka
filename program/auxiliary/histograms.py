import os
from math import log
from math import floor
from random import seed
import random
import numpy as np
import cv2
from scipy.interpolate import interp1d
from scipy import signal
import matplotlib as mpl
import matplotlib.pyplot as plt

minmax = True
TRUEminmax = False
DOWNminmax = False
file = 'podobnosti-shadows' # norm_podobnosti-shadowsDOWN, struktury, podobnosti, norm_struktury-shadowsDOWN 
title = 'histograms'
outpath = 'output/' + title + '/'

### EXTRACT FEATURES ###
img = cv2.imread('images/'+file+'.png')

width = 600
height = 400
height, width, channels = img.shape

min_const = width*height*0.001 # 0.1% z rozlisenia obrazku (asi mozem aj zvysit)
if (width*height*0.01 >1000): 
	min_const = 10

if (channels > 1):
	channelR = img[:,:,2] #R
	channelG = img[:,:,1] #G
	channelB = img[:,:,0] #B
	channel = ((img[:,:,1])//2 + (img[:,:,2])//2) # average of green and red
else:
	channel = img

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.resizeWindow('image', width,height) # neorezava, skaluje
cv2.imshow('image',channel)

# povodny histogram
hist = cv2.calcHist([channel],[0],None,[256],[0,256])

treshold = 120
maxval = 255;
(T, threshImage) = cv2.threshold(channel, treshold, maxval,  cv2.THRESH_BINARY)
cv2.namedWindow('treshIMG', cv2.WINDOW_NORMAL)
cv2.resizeWindow('treshIMG', width,height)
cv2.imshow('treshIMG',threshImage)

log_hist = [log(h[0]+1) for h in hist]
plt.plot(log_hist,color = 'blue')
plt.axvline(log(treshold))
plt.title('log_hist')
plt.xlim([0,256])

log_hist = np.asarray(log_hist)
log_hist = np.append(log_hist, [log_hist[-1],log_hist[-1],log_hist[-1],log_hist[-1]])
print(log_hist[-10:])
blur_hist = signal.medfilt(log_hist,5)
print(blur_hist[-10:-1])
blur_hist = blur_hist[0:-4]
plt.plot(blur_hist,color = 'red')

if not os.path.exists('output/'):
	print('creating path')
	os.mkdir('output/')
if not os.path.exists('output/'+title):
	print('creating path')
	os.mkdir('output/'+title)

i=''
if os.path.isfile(outpath+file+'_hist'+'.png'):
	i = 0
	while os.path.isfile(outpath+file+'_hist'+str(i)+'.png'):
		i+=1
plt.savefig(outpath+file+str(i)+'.png')



cv2.waitKey(0)
cv2.destroyAllWindows()















