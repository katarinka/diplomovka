import os
from math import log
from math import floor
from math import ceil
from random import seed
import random
import numpy as np
import cv2
from scipy.interpolate import interp1d

import matplotlib as mpl
import matplotlib.pyplot as plt

file = 'big_image' # podobnosti_denoised_bilateral-shadows struktury_denoised_bilateral-shadows  podobnosti, struktury, podobnosti-shadows, struktury-shadows
extension = '.tif'
title = 'divide_image'
outpath = 'output/' + title + '/'

img = cv2.imread('images/'+file+extension)

height, width, channels = img.shape

if (channels > 1):
	channelR = img[:,:,2] #R
	channelG = img[:,:,1] #G
	channelB = img[:,:,0] #B
	channel = ((img[:,:,1])//2 + (img[:,:,2])//2) # average of green and red
	# img[:,:,0] = 0 # BGR -> blue = 0
else:
	channel = img


if not os.path.exists('output/'):
	print('creating path output/')
	os.mkdir('output/')

if not os.path.exists('output/'+title):
	print('creating path')
	os.mkdir('output/'+title)
	

def divide(width, height, new_w, new_h, overlap):
	true_w = new_w - overlap
	rest_w = (width-overlap) % true_w
	num_w  = floor((width-overlap) / true_w)
	new_w = true_w + rest_w/floor((width-overlap)/true_w) + overlap
	true_h = new_h - overlap
	rest_h = (height-overlap) % true_h
	num_h = floor((height-overlap) / true_h)
	new_h = true_h + rest_h/floor((height-overlap)/true_h) + overlap
	
	return num_w, num_h,ceil(new_w), ceil(new_h)
	
new_w, new_h, overlap = 1500,1000,50
num_w, num_h,new_w, new_h = divide(width, height, new_w, new_h, overlap)

for h in range(num_h):
	for w in range(num_w):
		i=''
		if os.path.isfile(outpath+file+str(h)+str(w)+'.png'):
			i = 0
			while os.path.isfile(outpath+file+str(h)+str(w)+'_'+str(i)+'.png'):
				i+=1
			i = '_'+str(i)
		cv2.imwrite(outpath+file+str(h)+str(w)+i+'.png',img[h*(new_h-overlap):h*(new_h-overlap)+new_h, w*(new_w-overlap):w*(new_w-overlap)+new_w])
		
	
	
	
	